---
title: Pandora in the New American West
author: Erich Spaker
---

# Planning

## Tasks
- super hyper detailed description of Danny's box
- train terminology

### dad
things beta readers missed

## Notes
### Ideas
- [ ] suicide, pruning a non-branched reality
- [ ] jail in new pheonix \motif{west}
- [ ] second mole rat mention.

### Snippets:

the city it's... like it eating itself. -> pheonix

its strange... what is... that a place can be so far removed from itself that it loops back around to just being what it is.

"the money changed you" "for the better" he joked, her smile seemed to say "but it did" \resolve{ch27} or \resolve{ch26}

rancho: "you ever rob a train?"
"No never" forshadows c4 robbery

"I'm reasonably sure we aren't all dead. other than that I never bothered to check"

A smile is a broken thing. We broke it, somewhere along the way. Because we thought it could be everything. But it only covered everything. And then when the smile broke, we thought the world had broken, not that it was free. And the sky just kept falling, except it never fell, and the motor's kept on smoking, but never stopped. And people simply couldn't understand how the world was still running, with all the paint peeling off.

To leave it all behind, in the middle of the desert, at the bottom of the sea, for the city where a story's just a story and a train is just a train. 

and so it was that a spinner, even a woman of frail build and little knowledge of the world, could walk soundly down the dim lit back alleys, fate safely diversified

**Somewhere: a city on the eastern seaboard.** 
One or a couple days, down the line. -> Possibly add as another chapter between 5/6

Hylan would have rather eaten it, but she said not to be selfish -- easy for her to say, she'd already finished hers. \motif{selfish} \resolve{ch27}

Better than what?
_Better than you not coming back, moron! \motif{better-than}

She didn't cry though. Hylan had never seen her cry, well maybe once, maybe. \resolve{ch4}

A man with thick beard and hair dressed in all black with rings on his fingers rides with a naked boy of 7 through the sonora desert searching for those responsible for the slaughter of a local village
The deranged laughter of villians, not the typical evil laugh, but constant, essentially unending throughout the film, from the grunts to the big bad, they all laugh for minutes on end.

They had taken one like it, a long time ago: they had spun for those tickets. Now they could just buy them, bid up to oblivion on the resale market. It didn't matter. 

They took the steps down to the basement, through the non-descript door marked *policia*

why hylan? because of something along time ago
because there's only 32 cars on the trans american express

| so where you from anyway.
| 
| I told you, "somewhere" \motif{elsewhere}
| 
| *asi asi*, I'll quit asking.
| 
| no Sombwere, it's a city on the eastern seaboard.
| 
| Neuc?
| 
| just outside.
| 
| why'd you leave
| 
| just on vacation.
| 
| no hermanito... this is no vacation. está claro, you really runnin from those federales?
| 
| no, I didn't even know they were on me. I just--
| 
| --I just needed to get away...
| 
| He said it, and just then Hylan found that Rancho was a profoundly kind man. He knew he was kind, because Rancho had not allowed hint of sympathy or understanding to flash across his face. He just looked on. Eventually he said.
| 
| Well you came to the right place, this ain't nowhere at all

### 2nd pass { #2nd-pass }
- num chapters equals num cars
- ambiguity about whether box actually works
- footnotes
- decide use of profanity and edit

### conceptual pins
- "you can't put pandora back in the box"
- "just another day in the west"
- "what do you want, Hylan"
- "you cannot describe the superorganism in human terms"
- "future all his own"
- "the simulacra collapses"

### Visuals 
(these are things evokative enough that they could go on the cover)
- danny's box 
- danny with polaroid
- train with steam seeping out
- bar fuego (glass)
- 2 men staggering into dessert
- the LA sea
- (kinda) the gambling room
- tango
- boxes with cables strewn around the table

## Publishing
- [ ] pitch material
    - text:
        - [ ] snippets
        - [ ] chapters (2) 
        - [ ] epub / manuscript?
    - [ ] query https://queryshark.blogspot.com/
- [ ] email Archdruid
    - [ ] read book
- [ ] ask around
    - [ ] UR
    - [ ] publisher on east ave?
- [ ] search for other writers
    - ask around
