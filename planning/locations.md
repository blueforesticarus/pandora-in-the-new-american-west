## Location

### transamerican

\TODO port from notebook (i lost the notebook)

From [[#ch3]]

+ engine
+ faux coal car
+ observation deck
- 1,2 : ballroom
- 3, 4, 5 : sleeper cars
- 6, 7 : \TODO{sleeper and gym car}
- 8, 9, 10 : sleeper cars
- 11, 12 : cargo car
- 13 : wilber's cargo 
- 15 : hylan's car
- 16 : sleeper car
- 17 : lobby
- 18 : cafe
- 19 : dining
- 20 : bar
- 21 : gambling \st{b/c blackjack}

- 22 : kitchen

- X : music car (accordian)

- 31.5 : caboose \FIXME{this number needs to match }

Currently 9 sleeper cars, orient express has 12
could slip one or two in the back.. My original plans had crew quarters in the back
transorient has daytrip/coach

#### loc-coal-car
> \META chekov

- if [ch13] doesn't resolve it, then it must be resolved later. **when?** ch20 he has no reason to enter the train, maybe rewrite the tracks sequence.. except that doesn't work with hiding. 
- one way or another the resolution is *hylan hides in the false bottom*
- if we hit this another time (good idea) probably needs to be in the middle. He finds the false bottom before it is used. (much more obvious chekov). If he only finds it here, plays bad with the gun fight (too exciting). 

#### loc-cargo-car
> \META \TODO
- [ ] better set up cargo car switch / mystery
    - [ ] rancho scene
- [x] why is cargo car unlocked originally?
    - actually, why not. no-one can move the cargo anyway. Hylan has eyes all over the train.
      He is a hidden in plain sight kind of guy.
    - [x] made key instead of lifted it.
- [ ] make what happened explicit
    - [x] 2 cars in 13a
    - [x] (pick) lone star has identical cars 
    - [x] (pick) "spliced into consist"
    - [ ] "it wasn't his money which was missing, it was the whole car"

##### key

#### loc-bar-car

#### loc-gambling-car

#### loc-grand-hall

### new phoenix

circles -> baudrillard orbitals \motif{simulacra}

#### #4 and #2 { #loc-d4 }
> \OKAY

District 2 : "Medio
- "Medio" is where hylan confronts lucy. Symbolic half way point of the book.

District 4 : The unnamed desert opposite #2
- The #4 train is what hylan takes \fixme{to and/or from} #2. 
- "no trains run back from #2"

#### Trains

- #14 expreso
- #11 L
- #17 (connects 14 to 17)
- #4 (#11 to #4, lucy)
- N W cross track

#### loc-hangar

#### loc-hideout

#### de la muerte
see \motif{thirteen}

#### bar fuego

### LA
- is the \motif{destination}
- related to \motif{sidewalk-ends}

All glass bar in LA: Eden

#### la-sea
> \META \TODO

add refs

#### city-of-stars
This works well because its mentioned only twice. This is what wilber calls it, and then later hylan uses the same description subconsiously. \GOOD

Could add maybe one more use by hylan \WORK

