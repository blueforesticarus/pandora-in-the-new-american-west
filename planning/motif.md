## Lingo

add more lingo \motif{2nd-pass}

dorsal vs ventral continuity \ALT

## Allusion 
> \META \motif{2nd-pass}
- [x] talking heads
    - surprisingly there are 2 I'd hardly thought about
- [ ] andromeda
  - connected with Danny
- [x] age of absolute abundance
  - immaterial age
- [ ] aluzufera
- [ ] heart of the lag machine
- [ ] "interlude"
- [ ] witch and the phantom moore
  - O'Connor's
- [ ] the bean counter

IDEAS:
- [x] Layton
  - mentions of accordian also help. 
  - base exterior train descriptions on molentary express and real world orient express
- [ ] thomas the tank engine
  - had never seen such a mess
  - the magic railroad?
  - another good tongue in cheek reference
  - island of sodor?
- [x] Piazola
  - cafe mention 1930 (or 2030), with thematic overlap to song
  - hylan and danny dance to piazolla at some point
    - it could be their song.
  - Think about his more when doing \motif{dance} refactor
- [ ] Murder on the orient
  - I probably would need to at least watch/read this. 
  - should be tongue in cheek. Add later \DEFER
- [x] Woland
  - wilber can be introduced as a brittish professor
  - obvious method is to compare man in red to woland. (wilber is already similar name, and both are fake names anyway). Voland and Faland are accurate subsitutes.
     - I should reread woland's dialog to rewrite wilber's
  - also, can have man in red call danny "margaritta" \TODO
  - use "the master" somewhere?
- [x] Justina 
- [x] Woman from girl interrupted (lisa rowe or torrey)
- [x] Kandleman’s krim
  - literally just call some placebo kandleman's krim
  - perhaps the box itself
  - also could mention in stream of conciousness desert sections
- [ ] Cowboy bebop
- [x] Evangelion (kinda already do with NY3)
  - [ ] one mention of instrumentality would do it. \WORK{can I actually use this to convey meaning}
- [x] Where the sidewalk ends
- [x] El Topo

- [x] "They were native, from a town nearby, Mexicans lost in mexico"
  - maybe rancho chapter

## Imagery

### colors 

#### grey light
> \OKAY

pretty much tied to \motif{new-phoenix}

This is good. The motif is set up well by the many references to grey in 13a plus again in 18. Then 22 creates the phrase, reuses it for effect, and 25 resolves it. \GOOD

If anything, its overdone. \WORK

### phoenix
see: \motif{bar-fuego} and \motif{new-phoenix}

I like the phoenix motif, but want to subvert it. The world only feels like its going in circles.


Danny and Hylan are the phoenix. Phoenix represents a strange loop.

### two men cross desert
> \OKAY \DONE{A}

- [x] needs to happend on route into new phoenix
- [x] again while watching de la muerte (this is how hylan's suspicions confirmed)

Symbolism: 2 go in, one comes out. Inhospitable....

Three current uses closes it. \GOOD

### flash of light
> \GOOD \DONE{A}

I think this is good example of optimal lean motif development. 
It is used once near the beginning, then the pattern soon is made explicit with a phrase, the phrase is used again one time in the middle to remind the reader, and then once, naturally, at the climax and again, winking, in the conclusion.

#### ozone

### cityscape
> \DONE{A} \OKAY

This is finished. It should not be developed past New phoenix \GOOD

### accordian
> \OKAY

exists to facilitate an allusion to piazolla, and a ch13 callback

## Diction
This section is for patterns of diction

### spinners

\OKAY{added all missing links}
\TODO{create sub-motif for "spinners always did"}

Add to last chapter \TODO
maybe a whole bunch of times. (like every other sentence for a paragraph)

### ch1-cadence

And she slipped back inside, ... and a future they could share togeather.

### thick-as-thieves
> \META \DONE

maybe relate to \motif{danny}

### spanish

- fix spelling of expreso, styling of spanish in \resolve{2nd-pass}

### wait too long
> \GOOD

What does this mean. Hylan or narrator waited too long to act on his feeling once in the past, and now is always rushing, but then, hes still running away. 

This isn't really put forward as a moral, more like something people feel without it necessarily meaning anything. Either way "the heat goes on."

## Foreshadowing

## Characters

### Hylan

Hylan Redd relates to \motif{man-in-red} \motif{colors}

#### hylan-did-not

### Danny

Daniella Gray relates to \motif{grey-light} \motif{colors}

grey/gray grey default, gray for danny

This is the only character I need to be extra carefull with.

- A mirror of Hylan. The driver of his hesitation.
- Bipolar, proud - never revealing much, coy playful, girlish by nature but capable of grace. 
- Really a kindred spirit to Hylan, but strained by the lifestyle, just as incapable of escaping it. Codependant relationship.
- She is described as useless and manic, but shown to be capable. In the end you wonder if in really Hylan was the problem. 
- We know he can't be with her anymore, the twist is it's because he loved her.

Her death might feel out of place if \motif{dannys-box} is not fully developed.

Her death kinda weakens \motif{day-in-the-west} -> resolved by the double ending

Hylan loves Danny, but this is supposed to be a revelation at the end. It should seem to the reader that he is sick of her until the end, where it is revealed that his discomfort is actually based on a fear of seeing her die, or making her see him die. His offer to leave this life behind should seem like giving in, when really genuine. His thoughts of divorce, is actually an effect of desperation. \WORK

There is a risk of Danny being 2 dimensional. Austin portrays her a bit too much like a male fantasy. But that chapter is good, so mostly it should be counterbalanced by her being useful and independant later on. (We want to showcase both Hylan’s warped view of her, as well as the reality) \WORK

reinforce notion of her as a spinner prior to new phoenix. She and hylan need to spin for something "they spun for the..." not called attention too. \DONE

I think the idea with her, is that she wanted the exciting bonnie and clide thing, at first, but now she just wants him. He fails to realize this and the fact he feels the same. 

Her spanish is worse than his 

- [ ] maybe one more scene with danny in ch4 or ch3 \ALT

#### single-hair

#### white-oleander
> \WORK \allusion
 
- novel is about growing up, overcoming, overbearing mother, accepting family/ mommy issues...
- specifically symbol of a womans pride, could have danny wear it. or the woman on the train 
  - but is danny really like that, or only in hylan's perception?

#### spaghetti
> \DONE{A} \OKAY

Possibly to many instances

### Narrator
1. Implied spinner by the “we” in paragraph 1
    1. But later, maybe we refers to physicists or rancho
2. At first has no persona, leaks **occasionally**
3. First opinion direct statement of opinion is “word on neomexico”
4. Implied to be the one who sells binoculars
5. stated to work for rancho, implied to be watching Hylan
6. Revealed as a physicist turned poet
7. Knows more than he tells, but lacks complete picture
8. Implied to send danny after hylan saving him

* There should be allusion to the narrator's role in the story. Somewhere between an easter egg and not quite concrete. 

narrator POV and mood in \resolve{2nd-pass}

I think I am getting a better sense of what sets the tone. 
- 3rd person non-omniscient and when injecting thoughts, it's his poetic license.
- basically, the injection of thoughts is meant to convey things the narrator observes but can't easily explain visually. Or are directly connected to the point he is making (pang)
- may want to annotate all omniscient sections. For tone rework in 3rd pass.

### Man in Red
AKA cardinal AKA wilber \TODO

"where's wilber", can't be in climax, has to be earlier or walking back with mateo \ALT

\motif{colors}

simulacra villian, mephistocles, collapses by
- hylan becoming him / jack jowdowsky
- reaction to being shot
- nuke gambit, which completes spinner dream (logical conclusion)

#### cardinal
    wordplay mystery

The Man in Red is the same person as the frequently described "Cardinal" of New phoenix. 
This mystery should be made as critical to the story as possible. 
The only place this should be directly hinted at is early, the Austin chapter \TODO{make it more obvious that the comparison is to the bird.}

Revealing this in the ending is optional. (as is much of the ending)

I am aware that cardinal's live in Rome, and bishops have cathedrals. But I don't care. In this world bishops can be cardinals. Good footnote (I was half way through the book when I realized I had cardinals and bishops mixed up, with no good way to fix it. So I didn't)

develop intrigue 
- [x] cathedral fire 
    - hear about job in search chapter then later there is a fire, imply via wording connected \GOOD 
- job for wilber is bombing/robbing cathedral \ALT
- [ ] "pardon the mess, I just became bishop of Lago Angeles"

WHAT IS THE JOB FOR WILBER

#### devil

- [x] I want an allusion to woland in master and margarita

see: \motif{faust}

#### pacemaker

### Matrona
President St. Matrona O'Connors

O'Connor escapes assasination, looks like we are going to have one president again, "but it wasnt oconnors country was it, her country was just one in this world, and the world belonged to someone else" \ALT{remembered in confrontation}

#### Nukes
#### lone-star
> \META \OKAY

- [x] I like the idea of having it coming in right before climax and heading out during or right after.
- [x] Same cargo cars as TransAmerican

### Paz
kafkaesque

- [ ] his grave MUST be in LA, to resolve ambiguity of ch26 and to mirro hylan's path. 

could add a visit to his lab, as filler

### Rancho

I want rancho *mentioned* once before his bar fuego scene \ALT

### Lucy
> \META \OKAY

- [x] make lucy show up earlier, before train scene, \del{ideally multible times}
- [x] better foreshadow Lucy
- expand theme
    - "pay for things", comment feels out of place
    - \del{philosophy?} not much chance too
- \del{perhaps lucy should be investigating Wilber}
    - kinda has to be investigating him

### Young Spinner

### Justinia



## Culture

There is a general issue with alot of the book that the frequency of spinning seems out of line with how many are still alive. This can be fixed.
- Have hylan find out a spinner he knew is dead. (this should happen all the time and spinners don't get to know each other much as a result).
- Have it implied that there are tons of people dying all the time because of this. It is tolerated because it reduces overpopulation. People are resentfull because they lose family and find dead bodies all the time. From the outside it looks like suicide.
  - Remember that spinners want their bodies found!!!
  - description of police or other non-spinners finding bodies. \TODO
  - hylan finding and dealing with bodies \FIXME{important addition, ch17}
  - made explicit with Lucy, which about wraps up this theme
- Want to develop that two sides view. From he outside it looks like a death cult, constant suicide. From the inside it looks like invulnerability, and a nomadic life (you never see the people you meet again). Remember, it could all be fake.
- Part of this is selection bias. There will always be some people who win rock paper scissors 10 times in a row. Politicians who won all their spins are just the ones who happened too. There will always be a big wig in any reality.

someone should maybe literally spell out "all you spinners are good for is reducing the excess population" \TODO
I could better set up the population growth also \WORK

### neo

#### out old in new 
> \GOOD

I'd like to add one subtle use of the phrase early in the story. Played straight.

#### old fashioned
> \DONE{A} \GOOD

\motif{hylan} always orders old-fashioneds

Another possiblity is drinking with rancho, at some point. \resolve{ch22} \ALT

#### indomitable future
> \GOOD

### fucking spinners
> \META \OKAY

This makes very little sense for New phoenix - keep minimal

### smarter-lazier
> \DONE{A} \WORK

I don't like how much these two are similar \FIXME 
and I need one more variation

"He was getting sloppier" \ALT where would this go?.. 

"They're getting smarter", "or lazier" Hylan added (reference to rancho not taking box) \TODO 

"He's getting lazier.." "Or smarter" Hylan added (to rancho in reference to man in red, or another cop) \TODO must be in this order

### simulacra
> 

"What if it is a simulation, then what"

Danny's box is covered in smiley stickers, which themselves are often used as an example of simulacra \motif{dannys-box}

This could be made more explicit, perhaps someone has a copy of simulation and simulacra covered in smiley stickers. \ALT

- hylans leg has no impact on story ( but still feels funny) 
- he still cries when she dies
- LA 
- Transamerican (truly beautiful, truly fake)

ideas:
- disneyland obscures the fact the whole country is disneyland
- scandal obscures the fact there is no scandal

#### collapse
> \META \WORK

mom did not notice simulacra collase \motif{dad}

simulacra collapses in train at end, 
- because he is using it to get somewhere (as are some other people)
- because the fakeness is appropriate for LA, looping around to genuine
- leads directly into -> "Here dear reader we reach the end"

- [ ] not a spinner -> **"I am a spinner"**
- [ ] hylan was not -> **hylan was**




#### jack
> \META 
jack jowdowsky

- [ ] "I'm jack jowdowsky"
    - who do you think you are, jack jowdowsky? ch4 or ch11 or rancho
    - who do you think you are? "I'm jack jowdowsky" ch25 or rancho or wilber
        - pretty much has to be with Wilber if at all. 
    - not necessarily needed



#### faust
> \META \TODO

"A slight question, for one who so disdains the word"

"he who strives on and lives to strive can earn redemption still"

"a bright and lofty star"

"faust sinned and he was saved, just as the world sinned and will be saved, why? because we strive" wilber or justinia, or lucy, or danny \motif{snippets}

freedom from consequence -- everything is paid for

### americana
> 

### world-wide-spectacle
> \GOOD

## Motif

| xxx Continuum / Continuity
| 
| Danny being unhappy
| Glass bar in LA
| Edge of tomorrow
| A better place for them
| Assume they are there
| Youre just suicidal
| Wild West

### want
> \META

"What do you want hylan?"

- [x] danny
- [x] lucy
- [~] prostitute
- [x] rancho
    - this feels like too much \WORK
- [x] justinia

answer: a future all his own

"not even close, but you'll keep the cigar anyway" "of course, thats the point isn't it"

see: \motif{story}

### story

rancho arc:
- [x] "a good story"
- a story to tell -> tell who?
- [x] "you want a story, he's your lee van dick" 
- "thats one hell of a story" 
- [x] "it's not a story till it's over"

"you've got it backwards, its only a story until it ends"

story = simulacra, story ends simulacra collapses, not just a story anymore
see: \motif{collapse}

"it begs the question, when is a story more than a story"
"when it's happening--"
"or when it ends"


### clothes
> \WORK

So far hylan's clothes stop being mentioned once he gets to New phoenix. \DONE


#### hylan-suit-color
> \OKAY

Hylan's suit color subtly reflects change into man in red. (starts navy blue, changes to tan in new phoenix and is salmon in final scene) BE SUBTLE 

#### sunglasses
> \META \OKAY

rancho: will you take those damn things off \ALT

### polaroid
> symbol \WORK

Must:
- introduced [[#ch5]]. \DONE
- moth/out of film in [[#ch27]] \TODO

Options:
- its sitting on the counter somewhere (stand in for normal reference to \motif{dannys-box} \TODO
- [x] [[#ch7]] looking around at city, or Hylan takes her picture, she blushes
- [x] [[#ch9]] polaroid on counter as she reads
- [ ] [[#ch11]] picture of cathedral or picture of Hylan with mexicans at lunch \WORK
- [x] [[#ch12]] polaroid camera in her lap during interogation, when rancho looks away she sneaks a picture
- [ ] [[#ch21]] Hylan borrows polaroid to take picture of train
- [ ] [[#ch22]] view out of Rancho's office, or "smile boys"
- [x] [[#ch25]] around neck as they board 
  - maybe she is taking picture of skyline when hylan notices new phoenix.

Butterfly Effect
- Danny takes a picture of a butterfly in some situation highly symbolic of the butterfly effect. Could again be during austin dance, introduction of new phoenix, first chapter in new phoenix.
- Want to avoid overselling it. 
- "I want to tell you a butterfly landed on the luggage. I want to tell you. But I can't, it didn't. In fact the last time I saw an insect out this way, it was a confused bubble bee crashing into a wall." \ALT
- would be good to mention this in more detail earlier in story, but it probably doesn't work. The whole bit should perhaps be reserved for another story 
- **The polaroid is like observation in the quantum motif. What needs to be observed?**

Polaroid must be "out of film" on the ride to LA.
- "She pressed the button to take the picture, and then frowned, nothing happened, it was out of film."
- butterfly effect: some moths get into the luggage, which causes hylan to move the box, which leads to the ending. \GOOD

I can also imply that narrator uses a polaroid camera, which works with pre chapter polaroids which danny could not have taken. \TODO

#### butterfly

Moths are used to signify the butterfly effect. relates to \motif{sane-measures}

What is Hylan's butterfly effect mistake:
- [x] spinning in the first place 
- [ ] \del{hiding the money, taking the train}
- [x] agreeing to give up spinning with Danny
- [ ] \del{taking the box}
- [x] putting the box in the bag where the camera was

The most important is to have moths during confrontation with Danny. 
Because it is specifically his agreement with her to stop spinning which causes her suicide at the end. 
Taking the box, would have been fine, and losing the money is at best circumstantially related. 
Hylan's original spin is related because it's (probably) his ever-impending death which stresses Danny out in the first place.


### dannys-box 
> symbol device

He could remove the battery from her box and it ends up back inside. \TODO

I still want to make the box seem more menacing but there isn't an easy way to do it \WORK

#### pang 
> \META 

need to dial in the unsettling nature of the box

hylan feels pang every time Danny presses the box
- [x] ch3 (some sort of super light assoc)
- [ ] ch4 (gamblers, difficult)
- [x] ch 11 beginning (hard but good for narrative)
- [x] ch 6
- [x] ch 9
- [ ] pang in final chapter

Difficult with narrator \FIXME

possibly convert first two to footnotes \ALT

### cigarettes
> \WORK

This is the weakest motif in the book. I have never liked it how it worked out. But so much of the story is dependant on it now. 

Pros:
- the story needs the spinner gambit of writing down numbers. (its a good gambit anyway)
- it being a cigarette box makes sense as a prop and lends itself to the "for-show" motif
- gives wilber or others a great way to play into it (ask for cigarette)

Cons: 
- overloaded, used for poker, train, train again, desert, and in the finale
- something else feels off

One option would be to have hylan still have his box in ch 13, losing it after the gunfight.
I can still have the group of policemen get off. An issue is that I still want the "fsssh" sliding the box back and forth on the train. (because it is funny) solution - solution: gun fire breaks his box

I had an idea at one point, to have Hylan never rejoin the game in chapter 4. This is a nice idea, but makes man in red's introduction much harder.

Addressing gamblers in last chapter is a good idea \TODO

cigarettes signify addiction/dependance. So at end hylan has started smoking.

hylan "does not smoke" \motif{hylan-did-not} 

Danny does smoke, which is why he has them. she asks him for one in austin \TODO
cigarettes for danny is along the lines of "a bit of poison" or terrance this is stupid stuff. \resolve{ch7}


### binary 
> \WORK

Hylan thinks about, and uses, binary often. Obstensibly because it is the diag readout and operating principal of the box. The motif serves to tell the reader **hylan is about to solve a problem with spinner tricks**, or more generally is tried to how Hylan engages with danger.. so it also just hightens tension. The theme though, is that of binary thinking, over rationalization, trying to control for everything, etc. The natural conclusion to this theme, is that either *a third thing happens* or *there was only one option*. 
- "both paths were wrong"
- The cardinal being inescapable / undefeatable
- getting caught at end (subverted, as he is saved not by his box but Danny)

binary *could* play into ending (defecting), as could entanglement

plays with entanglement in the "both paths were wrong" bit of ch12

### elsewhere
> \DONE{A} \OKAY

idea: nadalooger -> nada lugar -> nowhere \ALT


#### assume-they-are-there
> \TODO

ambiguity about whether box actually works.

This is hard, it will come up in last chapter but other than that there's very little oppertunity to develop.

### entanglement
    lingo irony

In spinner culture entanglement is, on the one hand, a known fact of physics, while also being the subject of some superstitions.
Hylan, obstensibly, does not believe in the superstition, but resorts to it when stressed.

idea creeps up on him when he debates forcing his way on the train, and he become certain of it (though it is argueably rational in this case) once the money is missing.

- needs setup "what every spinner feared, one had to suppose, those with a poor understanding of entanglement were doomed to perish to the earth" \DONE
- is there anything so entangling as love / a woman (too overt, use as theme)
- could be supported in the middle
  - limbs entangled with hers \ALT
    - would have to be a dream. probably. to not be too explicit
  - hylan reverts on ride to LA "what, this superstition again?", perhaps when Danny suggests the pair of them are entangled, or some other such playfull thing

There is oppertunity to elaborate on superstition.
- danny could suggest that if the pair of them were to both press their box far far away, there would be no world where they came up different
  - hylan suggests this is simply because the other would prune, but she is certain they would never have too. And that if hylan were to die a tree would fall on her, or something.
- A story could be told of a couple like romeo and juliet which makes use of the above ideas.

### sane measures
> 

**control freak theme**

- [x] issue with repeat "all sane measures often fail" in \resolve{ch27} and \resolve{ch24}

It would be impossible to keep the cash safe on the train.
You can tell yourself you took every measure, you can repeat it over and over untill its true.
You can do this, and you can believe it, and maybe it is reasonable, maybe it's sane.
But so what?
All sane measures often fail, none the less. 

- lago angeles "any sane estimate would have had the place under water by now" 

### gravity
> \GOOD

Also could use a mention in \resolve{ch7} in \resolve{2nd-pass}

### egoism
> theme \DONE{A} \GOOD

Ayn rand is referenced twice. The girl on the train is reading atlas shrugged and Danny much earlier is reading the fountainhead.
Danny prefers the fountainhead to atlas. Perhaps Hylan should prefer the opposite (currently he has only "heard of" atlas shrugged \WORK)

Of note is that the girl on the train was locked up for being "a sociopath" to which she says "everyone in mexico is crazy"

The girl on the train accuses that spinners "aren't egoists at all". Which is an early idea from the writing process and one I have not fully understood yet.

Why aren't the spinners egoists:
1. They think they are egoists (bravely living only for themselves) but in reality they are phonys doing everything for show.
    - "super-egoism" is a play on words, it is meant to mean applying superrationality to objectivism (kinda what spinners think they are doing), but ends up sounding like superego-ism which implies oversocialization, doing things for show, or because you "should." This works as an observation about counterculture in general.
2. An egoist wants to leave his mark on the world, they leave the world behind, and not to glenn's gulch, just another world which they will abandon.

The book was always, in small part, meant to be a critique of egoism. From the inside, of course, because I am an egoist whether I like it or not.

So what is the critique:
1. You are always leaving people behind.
2. egoism can warp into paternalism (superego) like anything else.
3. spinners are a sort of second-hander, and could be a metaphor for rich/successfull "sell-outs" (who admire rand while embodying wynand). In this sense, egoism is subject to the same rule as any idea: you have to act on it.

With the overt mention in 27, and the throwaway mentions of the fountainhead and materialism, I am mostly happy with this. \GOOD

Another allusion is the name of president O'Connors, who may feature in the Witch and the Phantom Moore. Although completely recharacterized.

In this round of revisions, justinia's dialogue is expanded. 
The new ending is more overt. Hylan is plagued by self doubt and a 5 minute converstion with an objectivist gives him enough confidence to see through what he started and achieve his happy ending. This is realistic, it has happened.

Critically, Justinia goes through all the problems with spinner philosophy, hits on Hylan's self-deceit, *before* suggesting that he own it. She cuts a line between what you are (which you can't change and shouldn't want to change) and action (which can be whatever you want). And point out the folly in psyoping yourself about costs when you could focus making it worth it. 

As a result Hylan abandons his resistance about being a spinner. And reconciles this with his desire to stop spinning. In the final cafe scene he is both proud of being a spinner at content with leaving it behind, having found (in both cases) what was worth it. Hinted by narrative arrow to be Danny, this is reinforced in the alternative endings, and his distress at her death.

The acceptance of her death and choice to live it out is more faustian then egoist. But justinia's comments do fores shadow it, and her perspective is critical to his shift of perspective. 

**It is not conveyed well in the narrative, but the necessity of living out the timeline without her is nessecary for the want motif.** 
Being willing to suffer her loss is the proof that he loved her. \motif{want}

"Love is the means and practice of accepting risk of injury." \motif{snippets}
You shouldn't say it like that, you should say "love is pain", or something. Its sounds better.
"But that's just silly -- something else altogether"

#### selfish
> \DONE{A} \OKAY

consider moving "come now, don't be selfish" to \resolve{ch27}

alternatively this could be something rancho says, when asking for a cigarette \resolve{ch22} or start of \resolve{ch20}

### superrationality
> \TODO

"a cooperation few could know, with themselves" \ALT \resolve{snippets}

where to put philosophy
- [ ] mexicani
- [ ] rancho
- [ ] lucy
- hylan / danny
- [ ] mateo in desert
- [ ] rando in bar mirroring mateo ch27 or ch25 \ALT

### west
#### westphalia
#### duel
#### day in the west

Just another day in the west. 

Resolution: I see two options.
Just another day in the west. Nothing that happened was planned or orchestrated or particularly connected. OR The man behind the curtain. The man in red orchestrated everything. 
If I can combine the feel of both, then it would be perfect.
Does he accept or not accept the deal at the end? 
Actually I’m not sure I like the deal storyline at all. It is kinda thematically redundant with Danny’s death and it undermines the mystery of the character.

### aesthetics
> \WORK

AKA social engineers 

32. Continually undermine notions of TransAmerican’s quality
    13. Especially in late chapters
        5. Corporate speak starts in chapter 11.
        7. Should add something in chapter 9 undermine quality and another to bolster \resolve{ch9}
    14. Careful not to do this much before chapter 9
        8. Chapter 4 needs to reinforce notion of quality \resolve{ch4}
        9. Chapter 6 should reinforce quality. \resolve{ch6}

### edge
> \TODO

Standing on edge. Once Hylan, once danny.
Edge of roof works. The caboose, the obersvation deck by the coal car.

#### sidewalk-ends
> \allusion

The poem is about the undeveloped, unpolluted place where the sidewalk ends.

I want 2 verbal references, 
"down where the walkway ends" in hangar -- \TODO{meaning}
or
could be in reference to path into desert.
or
could be done straight, ex. a green park

one with the peer when they get off the train, this time literally "where the sidewalk ends" -- he reached the place where the sidewalk ends, but ironically this is \motif{la}, plays on themes of cityscape, simulacra americana, contra...

### dance
> \TODO

Want: several visceral depictions of dancing.
- should convey thrill

1. in added early chapter \ALT
2. austin \WORK
3. brief dancing in new phoenix into
4. ballroom, and parking lot, "the last dance" 

#### dance-anywhere

- Danny "doesn’t feel like dancing alone." \motif{danny} 
- "Without you, idiot" (could dance anywhere, with him!)
- "dont mistake the dancer for the dance" \ALT

## Unsorted


