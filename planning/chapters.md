## Chapters

### polaroid pictures

COVER: danny's box, with red button, conduit, and smiley stickers. Make from spray painted cardboard.
On top of "mattress" with wine bottle visible behind. Staining sheets.

- ch1: Hylan and others around table on movie set
- ch2: spilled wine glass, crime scene tape \WORK
- ch3: long distance view of the train 
- ch4: roulette wheel \WORK
- ch5: danny smiling with camera 
   - issue is she takes pictures? \WORK
- ch6: Hylan looking out the window
- ch7: picture of picture \WORK
- ch8: skip
- ch9: picture of hylan in cafe car taken by danny \TODO
   - hylan and danny in window
- ch10: skip
- ch11: far away view of new pheonix
   - or statue of la malinche (or use this later) \WORK
- ch12: danny's picture of rancho rancho
- ch13a: steam seeping out of train from back window 
- ch13b: \TODO
- ch14: black speck in the dunes? \WORK
- ch15: \TODO
    - narrator photo of hylan leaving hotel, or walking into desert, or wooden crosses in the sand
- ch16: skip
- ch17: notches in the doorway \WORK
- ch18: \TODO
- ch19: something taken by danny \TODO
    - la malinche?
- ch20: \TODO
- ch21: maybe skip \WORK
- ch22: for some reason I feel a wide paning shot of empty streets would work here \WORK
- ch23: skip
- ch24: picture of sun directly overhead, or something else signifying high noon \WORK
    - or danny in the blood soaked dress, or la malinche
    - or two men stagger across desert
- ch25: people waiting to get on train
- ch26: skip
- ch27: the LA sea.

1. a certain scientific deathbox
2. the immaterial age
3. the transamerican express
4. the man in red
5. gravity
6. whatever strange attraction
7. limelight
8. 
9. *la dama flor*
10. a word on neomexico
11. new phoenix
12. rancho rancho
13. entanglement
14. 
15. picture of the neo-normal
16. de la muerte
17. 
18. the black hut
19. medio
20. 
21. a deal at bar fuego
22. the lone star line
23. montage
24. 
25. climax
26. another day in the west
27. fallout
28. the transamerican express (reprise)
29. The true ending.
30. Elsewhere.
31. Somewhere.
