# Brainstorming {.unlisted .unnumbered}

Can I insert a bank robbery into the story.
Requirements:
- it is under duress. (hylan's money is there)
- has to be in new pheonix. right before climax

- could be man in red contracts him

there is a perfect scene of him wiring up a bomb

it could be during final chapter

Danny kidnapped?

alternate ending, hylan shoots man in red

Endings:
- hylan goes back to new pheonix to work with rancho
- hylan returns to bank robberies
- hylan takes deal
- shoots wilber
- kicks box into sea

the spinners weren't gunna spin, they were discussing waiting for another job, one that double crosses wilber?

or wilber needs hylan because hylan killed his guy

1. money missing
2. police station
3. finds hideout
X
4. kills lucy
X
5. works with rancho
6. rancho missing, waits
7. ambushed
8. confrontation at hideout
9. leaves new pheonix

---

Could be after Hylan gets the money. 

**Right now I am having doubts about both the second Rancho chapter and the addition of the bank robbery.**

This is the main thing holding back dev on the novel. However, the implications are minimal outside 12 and 13b
1. false ident
2. slight shuffling of 13a

There is some question if he has his gun.

Benefits are setting up false ident story better. Downside is that it feels less ambient, less western, less tension from 13 -> 14. Having prisoners dilemna in 13b ruins it for final chapter, where it would be more culminating. 

One option is to split it up.
1. move all of 12 to 13b
2. put false ident reveal in rancho rooftop scene OR add new scene before or at begining of ch15
3. relocate danny's end of hall bit to 27 OR insert somewhere???
4. paramedics ??? \TODO

This may be a case of having to kill your darlings.

Other stuff:
The biggest missing pieces now are ch11 (introducing new pheonix) and ch27 (the winddown)
Both will be hard to write without other parts of the novel crystalizing.
This is the final jigsaw.

Biggest unplanned motifs are: polaroid/butterfly, dancing, standing on the edge/where the sidewalk ends
underdeveloped: binary, aesthetics, all-sane-measures, 2 men cross desert

So the strategy probably should be to plan out motifs, write a very detailed plan for final chapter, then iron out the middle (maybe 2 versions), then get most of the chapters to a semipresentable state, then write final chapter, then write possible intermission chapter. Then do a final pass for every motif, then do a final pass for grammer and diction.

**I am also afraid of overpolishing**

Okay. So, I still don't really know how I want the ending to play out (wilber meeting and dannys death), I don't know how I want the end of 13a and 13b to go, or even if there will be a 13b. I don't know what I want to do with 11, or even if I want to expand it much at all. I don't know if I want to insert a bank robbery, or if so where. If I cut 13b, I don't know where to move it (in part or whole).

I don't know if the the direction this is heading will feel too ambient, too symbolic, or too "on the nose". I don't know how to evaluate this or what to do about it. 

So, *what do I know?*
1. I know that everything leading up to ch11 is good, nothing big can be added.
2. I know that there will be no additional chapters at the end, but ch27 will be long. 
   It will have a part with the woman, a part with the accordian, a part with dancing, and a prisoners dilemna
3. I know: train -> search -> hideout -> lucy -> danny -> rancho -> ambush -> duel
   The only viable places to expand are before the train, after the duel.
   (The early rancho chapter, and between train and search, have SOME wiggle room)
4. I know I want the polaroid motif.
5. I know something is better than nothing.
6. I know that if Hylan's leg is injured then it needs to have impact on story

I think I want a loose, non-symbolic description of new-pheonix somewhere. (as a general note, the story needs to breath) \motif{new-pheonix} \resolve{ch11}


I think I want the scene with hylan's injured leg, and I think it works better later in the story 

- How to avoid beating the reader over the head?
  Some things should feel poignant, but moreoften it should develop a theme without being notable.
     The only way to manage this is in a later revision state, but for now I want to aire on the side of being **rough** and subtle.

  I don't think I have too many motifs. I think motif dense writing is my style.
  What is possible is that the "hey look a pattern" feeling is evoked too often.

  That pattern is evoked via emphasis, the simplest example being the use of short sentances at the end of a paragraph. **We want to avoid winking at the pattern itself to become a pattern** Motifs is not a motif, and if it is, then it needs to be **timed with the end of the novel** The pattern is already set up with the first chapter. Its usage needs to be more restricted, building in the falling action/finale. This will lampshade the technique at the appropriate time.

## new thoughts 2/3

I'm now thinking that the whole sequence with the coal car false bottom could be moved to after ch 24 or to ch 20. The gun wound as well.

- if after ch 24, we omit the meeting between wilber and hylan to set up the job, and jump right from desert to job.

- otherwise we might want to insert a gun fight after lucy, that results in the coal car false bottom sequence (somehow? maybe?) and ultimately with hylan at hospital with rancho.

- another option is to do a flashback sequence during rancho chapter, moving 13b

**decision** I am leaving 12 mostly alone.

waiting on feedback on ch3


obeservation: some places could use tone adjustment, keeping in mind the narrators state of mind and goals. In a few places (last 2 paragraphs of ch3) the narration is a bit to storyteller-esc and/or colloquial. The narrator is a scientist, and a poet, not a storyteller.
The narrator at this early point in the story is still trying to come across as fully third person. \TODO{mind the progression of pov leaks}. Narrator tone will be one of the most serious concerns for the 2nd pass revision (pre feedback) and 3rd (post feedback). \motif{2nd-pass}

The narrator is only really telling the story for the hell of it, perhaps under duress, and is just having some fun with it, and slipping in some complaining. Perhaps he's telling an interogator, or someone in a bar. His real purpose is to aire thoughts. Maybe he's even telling a therapist. Ch 26 tells you why: he lost a friend, has seen alot, he's a man of science with a romantic bent whose become disillusioned with the world, and he sees it as his job to guard the "last true desert" which I suppose a symbol for spinner subculture, by telling this cautionary tale in which nothing really happens and which doesn't really have an end or an aim. \motif{narrator}

**could put 13b as flashback in 14**

## Tue Feb 13 02:36:03 AM UTC 2024
I am warming up to the idea of 13b and am thinking a part right before the rancho teamup arc could include the bank robbery, coal car scene, and the hospital part

The tone is changing, but that might be okay. However, the bank robbery idea, second coal car scene, and hospital arc are kinda exagerations of the book (robbery is too depressing -- pandora, hospital is too ironic, second coal car is too noir). I can get away with 1 easier than all 3. 

The feeling of the bank robbery is satisfying, it really gives those verge-of-tears feeling. Hylan can't escape.
But the book is not meant to be that overt. And the ending is already like that.

\NOTE I could maybe get away with it if I was very subtle, glossed over it

The hospital part is less aggregious. But is a bit redundant with the obligate c27 prisoners dilemna.
At this point I think the tonal/pacing issues are fine.

The second coal car scene idk

## Mon Feb 26 10:06:22 PM UTC 2024

"I spent the better part of 2030 in a cafe in some nameless city in the northeast.
Actually it lost its name a long time ago. Long long time ago. 
I met a woman there who liked tango. Cat earings and a dress the color of white oleander.
The girl reminds me of her, perhaps thats why I paid so much attention.

I'm banned from that cafe. Friends of mine are banned from the whole damn city.
Who cares. Not us, a poets never do, anathema being like anything else: we'll take it and make something new: just another thing to write poems about"

- [x] Idea to have narrator start going first person randomly -> "personally I think the whole lot of it is..." "kandleman's krim" I suggested.. would have. \motif{narrator}

## Wed Feb 28 04:45:17 PM UTC 2024
Okay, I have annotated and mapped out most of the motifs.

Whats left to do is map out plot.
- [x] bank robbery / job for wilber
- [x] hospital rancho 
- [x] second coal car scene and/or finding car 13
- [ ] rancho wilber reveal
- [ ] rancho reveal of missing car 13 \FIXME
- [x] setup on hylan's tail
- [ ] setup on lucy and cantina
- [x] sunglasses/sombrero + hylans suit color
- \del{more on what Danny is up too}
    - she is like a ghost
- [x] **stuff at the beginning of new pheonix**

## Thu May 30 05:21:46 PM EDT 2024
notes from mother:

- [ ] did not understand the car switch
    - needs to be set up better, plus more explicit
- [x] did not understand gunfight, why was Danny hurt?
- [ ] did not pick up on wilber's scheme
    - [x] I might want to move reveal of wilber's scheme to another part of the book in a dream or something
- [ ] thought ending was too abrupt, timeline's confusing
    - I'd like to perhaps set up a job for Wilber
- [x] observed Danny is "like a ghost" 
    - [x] could include throwaway line near start of book "hair like a ghost" or something
- [ ] couldnt tell if it was a happy ending, sad ending
- [ ] felt voice alternated at beginning
- [ ] couldn't say what happened
- [ ] part of the story where I only have bits and pieces
- [x] finds actions of the spinners not believable
- [ ] **seemed like everyone they meet is a spinner**
- [ ] better as movie

TODO:
- [x] I don't like the spare box bit. (can have him use danny's box)
   - only bad by rule of thumb. It feels right here.
   - lack of consequence is a theme and tone for the story. plus \motif{sane-measures} 
- [x] hair like a ghost
- [x] could have dream where wilber and hylan have a duel
   - whether hylan wins or loses, the man in red persists, and the dead body has hylan's face.
- [x] moths in New Pheonix.
- [x] could have Hylan feel a pang then relief prior to every time Danny's box is noticed.
- [x] hylan could compare himself to a fictional cowboy throughout the book
    - does this fit with the themes/character?
    - con: how does narrator know. 
    - [x] connect to dream? "a dream he'd been having for years, he's in the desert like <name>", second one in tunnel
    - [x] possible to connect to the end. Man in red could be jack jowdowsky. 
        - or he asks Hylan who he thinks he is and he just says Jack Jowdowsky

- [ ] more anti-spinner strangers
- [ ] finish cigarette in the rain (sprinkler)
- [ ] more wilber / cardinal intrigue

ending: 
- [ ] job for wilber at end.
- [ ] add implication of dead man switch to man in red's description. (Claimed to be a pacemaker. -- pacemaker for the world)
- [ ] philosphical riddles from man in red. Super into superrationality. 
- [ ] duel scene in hall. mirroring dream
- [ ] Hylan inner struggle on whether to accept (defers to cigarettes, but they are out of digits)
   - this is symbolic, he's been hedging this whole time, but in a way this is just letting fate take him
   - resigning himself here is a recognition that he hasn't really been hedging so much being passive, he remains passive here.
- [ ] leave Man in Red's mechanism mostly implied by dream
- [ ] make man in red a more supernatural force (possibly have him already spin with hylan but somehow still be alive)
- [ ] man in red makes comment alluding to O'Connors, calling her the witch
- [ ] Hylan is going to shoot him, but thinks better of it (nukes, wires tied to his arm)
- [ ] Man in red proposes a job. Either hylan accepts and they spin for the world. Or declines, and Hylan spins for their lives. 
- [ ] Hylan feels something (like something being severed) and hears a far of noise. associated with conversation but implied later to be Danny's demise.
- [x] Man in Red has tied his heartbeat to the world. 
    - dance with death
- [x] read faust chapters for inspiration
- [ ] "would you like me to explain" no
- [ ] he needs danny's box (either for spin or decision)
- Hylan has already furfilled his purpose for Wilber, what is it?
  - should be something simple and dumb
  - wilber just oppertunistically used him
  - money mixup circumstantial, but wilber obviously knew, 
  - hylan effects: rancho, goons, something with police.
    - police know about train car.

   - and my role in all this? "circumstantial, and complete" this might work
 - [ ] "how about another job" and then "another job" and when all the jobs are done? **"I'll spin you for the world"**
 - mention of woman at the ball should annoy him, because it is tempting despite loyalty to danny
   -  that woman is a foil of danny, somehow
 - ~~imply wilber already hedged giving hylan back Danny's box~~
 - [x] Some unknown timeline -> all glass bar.

 Make it clear that in the timelines we are shown, either Danny dies or Hylan becomes Man in Red.
 So that the "all that can be is" is driven home better. Could mention both their boxes at the bottom of the sea.

## Tue Jun 11 03:44:12 PM EDT 2024
biggest issues:
- [x] I like the dream idea (and character idea) but they further undermine the pov of the narrator
- [ ] lucy needs more foreshadowing, and it might be better if she is investigating man in red, or cardinal
- [x] hylan pang every time danny uses box.

themes:
- new american, pandora, west are done well.
    - west could be helped by dual imagery in hallway at end with Wilber
- simulacra collapses
    - simulacra collapses right before the (divergent) end of the book
    - main issue is tonal shift from climax to the collapse of the simulacra
- "I'm still here, aren't I" 
    - is pretty well taken care of by the multiple endings.

Story starts with Hylan getting a life changing amount of money. He loses his money, fights with his wife, they agree what they really want is to start over, he gets his money back in a final duel, they go back to enjoying their vacation. But he can't escape his past and his deal with the devil comes back to bite him.
But that's just one story, in another it's happily ever after.

The point. Roughly, is that life is not a story, its a multitude of overlapping stories. **Real life happens where the story stops being just a story. When a train, stops being just a train.** Just another day in the west. Just another day in the future. "At the top of the mountain, at the bottom of the ocean, where a story stops..."

easy:
- [ ] fix sunglasses
- [x] tweak gun fight
- redo ending to be vague, as above
- \del{(maybe) rewrite to Hylan uses danny's box in ch13}
    - [x] but the spare plays well with inconsequentiallity
- only two cars in the hangar scene (I've decided this is the best way to do it)
    - note the emotional payoff of finding the other cargo car is lesser because he already has his money
    - plus, why is the other car still hidden \TODO
- add pacemaker to man in red introduction and associate with nukes at end
- better justify taking the box. before/during/after the scene where he does
    - "just in case we need it"
- [x] more references to "the web"

## Wed Jun 12 02:32:35 PM EDT 2024
another reformulation, things to reinforce:

- narrator
   - narrator tipped off Danny (Hylan has three tails)
- nukes
   - via pacemaker in early chapter
   - pacemaker at end
   - cargo? at end
   - [ ] add lone star line in 2 places in the end of ch25
      - same cargo cars as transamerican
   - [ ] brilliant flash of light is nukes
- lucy
   - [x] foreshadow (difficult)
   - [ ] revise dialoge in confrontation
      - maybe smile dialog, some genuine connection with Hylan (just barely hinted at)
   - [ ] tweak mentions in last chapter
- cardinal connection
   - maybe lucy investigating cardinal \ALT
- wilber intrigue
   - helped by dreams (red blood)
- avoid abrupt ending
- motive, and what is going on
    - [x] tweak gun fight
    - [ ] "first, he needed a box" + make motives in ch15 more clear \WORK{revise}
- danny's death (pandora)
    - hylan feels pang whenever pressed
    - add in ref to andromeda (where?)
       - picture hylan buys? hanging in hylan (as man in red)'s office
       - the painting in rancho's office is alot like the one hylan bought
    - more foreshadowing (like "how hylan would remember her")
       - perhaps he starting thinking about her safety... second hangar scene?
- polaroid
    - one more ref ch27, maybe on counter, or she takes picture.
- danny 
    - maybe she goes with him second time to cargo car.
    - maybe she's with rancho when they find him
    - photo from her day (not told in story) would be ch19
- "like a dual" / western
    - ending
    - climax
    - [x] dreams + character
        - use first  person for dream, reference el topo, 2 men stagger into desert, and hylan becoming man in red \GOOD
        - two dreams
- car switch
    - only two cars in the hangar scene (I've decided this is the best way to do it)
        - note the emotional payoff of finding the other cargo car is lesser because he already has his money
        - plus, why is the other car still hidden \TODO
    - Hylan's money is in the cargo car \ALT
    - Hylan finds cargo car with rancho earlier on \ALT
    - finding car and mailing Rancho is currently weak \FIXME
- danny's box handoff
    - [x] "something that should have been locked away in evidence in the basement of the station"
    - [ ] could reorder
    - [ ] better justify taking the box. before/during/after the scene where he does
        - "just in case we need it"
- simulacra collapse
    - \TODO
    - smile dialog
- setting consistency
    - [x] police station in basement of train
    - [x] more refs to "the web" (added one)
    - [ ] "It looked like something, like what he remembered of the LA Sea, not now, no -- long ago, when he'd first seen it."
- mexican woman
    - add convo
    - "you should smile more" (or lucy)

when these are done, send to Fred/Dylan

## Thu Jun 13 07:10:40 PM UTC 2024

Idea from dream was to have characters in the book talk about philosophy of spinning like 

"That isn't me though, that's some other person."


## Wed Jun 26 11:00:23 AM EDT 2024

The great problem occurs to me, my novel -- well written, compelling, inventive, beautiful even -- is not heroic. The ending is, at best, bittersweet, and not even that... sweetbitter perhaps. Hylan struggles between escape and maximalism, in the end he fails to escape, and he declines maximalism? Or maybe it's both. He gets both.

There must be the heroic. 
- kicking the box into the sea
- shooting wilber
- becoming the man in red
- happily ever after

We must get the sense that all these possible paths, Hylan has achieved the heroic. 

So what is the story. Our narrator, as a scientist and as a poet, tells us a story of a man. 
Why, because it is true and because it is beautiful. It must be both or neither, and then it would not be told. 

Hylan Redd is a bank robber, an egoist, a spinner. He is a man. And he has a woman. 
And they run away together. They run away from themselves while remaining themselves.

His money is taken, and regained, for reasons beyond his control and which he is too stoic to be troubled over. Except for this demon, this nagging feeling that he can never escape, because it is what he wants. 

And that demon appear to him, it taunts him, because he automatically disregards him, because he desperately wants not to be him.

So what would be heroic. No, that is the wrong question. 

How, how would he be heroic. When himself and the world are a lie. 

By making the lie true, lie until the very end.

An image, of pointing the gun at Wilber. but this wilber manages fine. But something, something must break. He must make Wilber afraid! But how does this lead to Danny and the box?

Revolver.

"Not smiling now? Are we."

"A smile is a broken thing..." 
So, just a crack in Wilber's paint. And it's enough. Hylan laughed. 

And then, all that could be is.

---

But somehow I do not feel that the heroic end fits.
Why, what is heroism. Well, certainly not an end, for one.
Heroism comes from suffering and ones indifference to it at the behest of ideals.
What are Hylan's ideals. He has none, besides the spinner creed he doubts heavily. 
What would be heroic. Fighting the world and winning, sacraficing one's self for his ideals. Has the box not robbed him of his chance at self sacrafice?

Perhaps he needs a real duel. For the simulacra to collapse. But that would be renouncing his spinner ideology. He needs it to collapse into reality. 

What could he do, he could rush back to danny before she can push the button.
He can shoot the man in red.
He can play real Russian Roulette with himself, then drop the gun on the table. (but isn't this normal for him)

What does he want. He wants to be something, to never lose, to be the man in red. He has his pride, pride that transcends death in one world. Superficial pride, pride that he's willing to push the button, pride that he's willing to leave a world behind. That the only thing he value's is his experience. But his experience is fake. A fake world.

What is the spinner box a metaphor for. Big risks? cutting off opportunities, burning bridges, the pioneer spirit, a lack of sentimentality. Who are the spinners of our world. The corpos? the sell-outs? the influencers? Or are they the artists, the travelers, the free spirits. 
Neither? Both?

Why does the reader care about the spinners

Everyone. The spinners are everyone, aren't they? 

So what would be heroic for Hylan, the world is designed to make heroism cheap.

What do you do. You do anyway. So what does Hylan do. He lives. He finds a principle. He makes a choice. 

"You are a spinner aren't you Mr Redd?" Yes, but not with you.

Hylan must find a princple. Does he love his wife, and want a family. No, that is running away.
He must find a princple. He resolves to find a princple.

"Its about time I started making choices. It's time to peal off the stickers don't you think."

"It's about time. Peal off those stickers won't you" "Yes, Hylan"

## Mon Jul  8 09:45:35 PM UTC 2024

pandora -> the only heroic thing can be to embrace the chaos
you can't put pandora back in the box, but why would you try.

you get n endings
- sad ending
- c4 ending (train hiest, cathedral hiest?)
- man in red ending
- end of the world ending
- happily ever after
  - which narrator chooses to put last

which is the one narrator is in
- he never checked, he never cared to know
- "banned from that cafe" -> "I never bothered to know"
- something relating simulacra collapse

the simulacra collapses, all that can be is, pandora's debt.
- somehow symbolically hylan kicking box into the sea secures his happy ending (in an alternate timeline)

I really want the scene with the man in red in the room with the robbers.
Could be after climax (another job -> c4)

Some big changes might need a third round of editing.

**Could add in jobs in New Pheonix to get closer to money**

- [ ] must include bit about things being earned somewhere

#### narrator insert
I told you, Hylan Redd is the one you want to know about.
But I was being facitious, you only came here with questions about the devil, the one you really want to know about, 
is the girl. Fair enough.

I don't actually know.
I'm reasonably sure we aren't all dead. Other than that I never bothered to check

But I took care with the order.

Meanwhile, somewhere, 

"So when does a story stop being a story"

"When it ends" / "You got it backwards, its only a story once it ends" (actually I like this better earlier in story, so the real point is that stories are stories when they are happening.)

"Happilly ever after?"

"could be" / "somewhere"

and it was.

alt: Among the many lifelines, they lived happily ever after.

## Thu Jul 11 06:03:49 PM UTC 2024

Alright, another day. What do I want to accomplish.
1. a few small things
2. a clear picture of the changes
3. a sketch of the ending

So the ending is, 
- one ending which is end of the world
- several bad endings in which Hylan does not prune out.
- one good ending which he earns by the above

- For this to work, I need to reinforce the pruning out thing.
    - why prune out (lucy or mexicani)
- For dannys death to work I need to reinforce the divorce angle. 
    - scared of her -> scared for her
    - also need to reinforce pandora
- For nukes to work I need gun misfire and pacemaker
    - and I need lone star line to reappear
- Job for wilber
    - perhaps a random one, keeping with theme
      "devastating"
- becoming man in red is set up by dream and narrator
    - and spinners piloting worlds speech
    - pandora theme
    - need to justify:
        - why is hylan willing to become man in red
        - why does man in red want him to be
- Happily ever after is set up by same thing as dannys death, and also by almost everything in the story

Things that need to be fixed
- number of cars
    - the entire train car has been taken in 13
    - replaced or swapped ?
    - after climax
        - can finding train car be earlier?
        - [ ] "figures the devil'd wait for the last moment"

- cardinal / catholic
    - add cathedral after climax and man in red watching
    - fire at cathedral
    - people paid by cardinal, perhaps mateo says something
    - bishop vs cardinal
    - lucy? no, I like lucy being there for him. It reinforces that he is in fact a signifigant person.
    - "they heard alot about a cardinal"

- hylan's signifigance to wilber \FIXME

smaller:
- lucy
    - she can show up earlier
    - she could have more foreshadowing in scenes she is in (smiles at hylan)
    - she could have more pre-reveal dialog (smile)
    - she could have more post reveal dialog
        - philosophy, arguement, trickery, or a moment of genuine connection
        - "thats not me, thats some other person"
- how cigarettes play into ending \TODO
- rancho
    - perhaps rancho is dead, promoted, or has become man in red (goal unknown)
    - [ ] raise question when mailing map

Narrative structure is solved, all that is left is the actual plot.
- Wilber uses transamerican to transport contraband using an unregistered third cargo car, which he has access to as a catholic cardinal (catholics helped finance the train). Wilber is trying to tie his heartbeat to the world (but he is willing to spin with people who will take on his role to keep the character going), from these spins he ammasses identities, and their wealth, connections, etc. At some point he gets involved with O'Connors, who makes a deal with him (likely involving his facilitation of the deal with narrators friend, missing president of Louisiana), that results in wilber acquiring nuclear weapons, which are stowed in the cargo car after it departs new pheonix and set to go off if he dies (the reverse pacemaker).
- The cargo car is seperated from the train and hidden immediately upon arrival to new pheonix, hence why Hylan does not find his money. He does not notice that there is a car missing. Rancho suspects the gambit but the official manifest lists only 2 cargo cars, hence his question to hylan.
- The train is sabataged to give the lone star and O'Connor time to complete their part of the deal. Once the nukes are passed to wilber, the train begins repair and soon sets off.

- Wilber tries to kill Hylan because?
    - working with rancho? (rancho also disappeared)
    - [ ] insinuate that rancho met same fate
    - [ ] insinuate carginal? then reveal its wilber
- Hylan goes to find cargo car because? 
    - holding up his deal with rancho?
- How does hylan get in at end to see nukes?
- Hylan's role was circumstantial and complete
    - circumstantial: his money happened to be relocated, so he happened to have to persue wilber's people and happened to have to work with rancho.
    - complete: he already spun with wilber?
        - wilber spun for him
        - they made a deal in new pheonix for job

ALT: hylan hides money in different cargo car, because he can't get into Wilber's. Once the first cargo car is taken, he checks the wrong one.

difficult to convey:
- wilber's gambit (easy)
    - superrational connection
- why hylan willing to become man in red
- why man in red wants hylan
- that Hylan has done job for wilber in another timeline.
- that bad endings (and not pruning) earn hylan his good ending
- **spinner philosophy (super-rationality)**
    - and it's connection to man in red's philosophy


I think the pandora theme is complete, by the sacraficial aspect of the ending.
- The new issue is how to connect that to simulacra collapse.
- on the surface: 
    - simulacra collapse = wavefunction collapse
        - **A simulacra collapses when you look at it.**
    - simulacra = story, quantum => all possible stories
    - one story is selected (story we are in), it stops being just a story, when it ends (life keeps going)
    - a train is just a train (changed this in author's note)
    - Danny critical to the simulacra collapse (have a **baby**)    
        - They will never spin again if they have a kid. Human instinct.
    - pandora is chaos, "chaos is information", information is communal / communication
    - "I'm Jack Jowdowsky" by saying it, it becomes true. Instead of being a subconcious simulacra of jack, it becomes true as he acknolweges the simulacra. 
        - but you are a spinner "yes"
        - "But Jack wasn't a spinner" man in red is jack
    - need to make simulacra discussion scene better
    - "who do you think you are, jack jowdowsky"

What is the singular idea: it's just the sci fi gimmick. (sci fi is, after all, a crutch)
What else:
- if everything is simulacra, it has to collapse into the genuine, when the stories that make it up end.
- put another way "I'm still here, aren't I"
    - might want to swap with "it's just the story we're in"

## Fri Jul 19 03:03:23 AM EDT 2024
- current idea is to have drinking, cards, discussing a job, and wilber shows up, in canteen after climax
- could have jail for west theme

- [ ] reinforce "just suicidal" in ch27 prior to confrontation

## Thu Aug 15 05:19:03 PM EDT 2024
This book has more of a point than I thought it did 
- it looked like the sea
- narrator choosing to live in new phoenix
- "it was a free country"

I am getting better at revision, understanding the narrative intent of a section I already read and what can and cant be inserted on a deeper level than flow. As well as understanding flow better. Flow can normally be reworked, it just requires edits to surrounding sections. But function is fixed, and adding something to a tight chapter is hard. 

Chapters are vignettes. 

However, understanding this, I have a chance to insert things the best I can, and trust I can accurately remove them later if needed.

An additional chapter may be required. But where.

## Fri Aug 30 01:15:42 PM EDT 2024
- feeling better about splicing early search.
- prefer latter placement for cathedral fire
- really want "glass shimmered"
- bearish on mateo monologue

- need to work on story/want in rancho sequence
- need to decide whether to attempt post climax bar scene (could push to ch27 optionally)

new theory:
- chapters must work totally independently (anthology feel)
- chapter transition benefits from alternating. I don't have multiple points of view so I do this with narrator inserts. **dry chapter endings lead to poetic chapter beginnings, and vice versa.** Likewise, alternate Hylan and setting descriptions.

## Tue Sep 10 01:12:34 PM EDT 2024
Thematic summary. 

This is a Faustian story in which the redeeming element is the rejection of self-doubt. 
Deciding actions separate from identity. 
The root being "because I wanted to"

The simulacra element (as applied to the wild west, america, the train, clothes, and spinners themselves), is related because the transition from "because I am X" to "because I want X" is a simulacra collapse. 

Notice how it is precisely the maximalism of LA, which collapse the simulacra.
While it is the spinner maximalism which collapses the simulacra of Hylan's identity.

The simulacra digs in, the more you try to make it real. Think of modern art, trying so hard to be original yet ending up totally derivative. \motif{snippets}

The solution is to accept the imitation as an imitation. Accept persona as just persona. And somehow it becomes more real. 

Incidentally, this is good advice to me. *Stop trying.*

I admit that, if I had known from the beginning that I was writing a book about simulacra collapse, I would have written a fairly different (possibly better, definitely more focused) book.

## Wed Sep 11 06:28:06 PM UTC 2024
Notes from rand:

Novel suffers from weak plot-theme. 

Theme is as above: the collapse of the simulacra into sincertity. 
Plot-theme is: a man runs away from his past on a train that goes in a circle.
climax: (I honestly don't know how to explain this.)
By using the same power he wanted to leave behind, the man creates several of timelines, several where he never escapes, returning to the beginning, losing his wife, or dying himself -- and one where he gets off the train starting a new life with his wife, protected from legal consequence as part of his deal with the villain.

Because the book is dialectic, and it's narrative engine is dissonance followed by synthesis. Hylan's conflict (is he a spinner or not) is not resolved because it is the *wrong question.* (what does he want, not how he tacitly ignores this throughout the book.)

Theme: romanticism vs realism
plot-theme: 

theme: not knowing what you want, when you have the power to attain it.
theme: identity vs desire
theme: love
plot-theme: a man is torn between love for his wife and fear the harm their lifestyle will bring her. 

it is not evil but knowledge of it, which doomed adam and eve \motif{snippets}

The book cannot have an altogether tragic ending, because it undermines the theme (and the faustian / dialectic structure)

The book cannot have a real happy ending either, because Hylan is not an ideal man, he struggles internally, and if very misguided in how he thinks.

There is no other character which can serve as the hero, so its up to Hylan.

But it is too late for Hylan, the same way as it was for Wynand, all he can do is channel it, long enough to secure a better ending than he has any right to. \motif{hylan}

## Tue Nov 26 04:54:42 PM EST 2024
baudrillard's orbitals: orbit meaning, networked existance lacking a center
ex) suburbs orbit cities while being disconnected from them socially/culturally \motif{2nd-pass}

## Mon Jan 13 10:40:59 PM EST 2025
Writing down for posterity. 
The only writing that ever made me cry while I wrote it. Was in ch28, the train ride to LA.
As danny decides to quit drinking and smoking, so she can have a baby.
I think it was the line "I've been thinking I'll quit" or perhaps "what kind of mother would I be"
which, knowing the end (of the chapter) as as I had already wrote it...

I don't know if the happier ending undermines this moment.
But to me the novel is really summerized in those two lines of dialog with danny.
"maybe we could have a baby" and "no. what kind of mother would I be"

And with that, all denial that Danny is a character becomes impossible, I suppose. 
I hope she can recognize a love letter when she sees one.

