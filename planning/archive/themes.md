
## Themes
(copied from gdocs)


* Wild west
    * Wild west stories are about the world changing and outgrowing its need for rough riders. 
        * New phoenix is a city split by** sand walls** between modern skyscrapers and adobe slums. 
        * The **desert** is literally being surrounded and shrunk. Inside and outside have been flipped around.
    * Phoenix, keep on rolling, just another day in the west. 
    * Wild westphalia: (wild west **subverted**) The play on words here refers to the **peace of westphalia**. This is symbolic in multible ways:
        * it alludes to the fact this wild west is in stasis, always dying and never ending (like a phoenix). Truly, **“the old had gone out without anyone asking it too and the new had stubbornly refused to bloom”**.
        * The spinners of the new american west have found a “peaceful” alternative to the duals of the old west. **“Back then a group like this would have turned on each other by now”**
        * Subverts the wild west theme. Spinners are pretentious and well dressed. Danny, too uppity for a cowboy's wife, is more of a gatsby's girl. 
    * Western storylines:
        * Being on the move, and getting stuck in one place are classic components. As is moving on again once the story completes. 
        * Kidnapped police girl has a western feel as well. 
        * The “shootout” at the hideout. 
        * Traversing the desert
        * The overarching story: Hylan being** forced into a** **dual with a darker version of himself**: the man in red.
        * Danny is a cowboy’s wife, a comfort, waiting at home for him. But under it all tough as nails.
    * High stakes. Desert, missing money, police, poker gambit, broken train, police woman’s murder, danny suicide.
        * Subverted: westphalia. Peaceful duals, deaths undermined, austin. 
* Simulacra Americana
    * This relates to the “**neo**” theme. The story does not take place in phoenix, but new phoenix. It does not start in New York, but New New york. **The only original cities mentioned** which Hylan has visited are (currently) Austin, Miami, and LA, (This may need revision). The fact that **Hylan is _from_ Boston**, not neo boston etc, is symbolic.
    * The “new” in new phoenix is redundant. A nod to this brand of absurdity. Doubled down on by “the futurist states of neo-mexico” which apparently even contemporaries could not take seriously.
    * **Bar** **Fuego**. Relates both to a phoenix (**fire**), and is a very Americanized name and place. Having multiple identical undiffentiable bars around the city (“he knew there had to be one eventually”) lends a surreal feeling. Those inside the simulacra (ex. Ranco Rancho) look down on the real world from above (rooftop). Meanwhile, it is entirely superficial, see through (all glass). And the only thing keeping the floor (what supports it) from being entirely superficial (glass) is a lack of money. What they strive for is an ideal of no substance, held up by nothing: Wealth in a vacuum.
* Pandora
    * You cannot put Pandora back in the box. **Hylan wants to leave the spinner life behind but he struggles.** This is also a common western storyline, although more often it is a cop, not a criminal, in westerns who struggles to hang up the badge.
    * Hylan “**doesn't understand**” gambling, drugs, or love, so he claims. Yet he is tempted by all of them.
    * There should be an element of cheating death, or that even death cannot free you. Symbolic to Paz’s death in LA.
    * Deal with the devil
        * From any non spinner point of view they are insane. See gambling scene, and female detective.
* “A spinner always does”
    * This is the main sci-fi element of the book. The hypothetical social consequences of a scientific breakthrough. The spinners have a culture that arises from the tech.
        * Dignity and respect, since backstabbing no longer has any purpose
        * Fancy clothes because they want to look good as corpses (ritual)
        * Mixture with high society, both because they think highly of themselves and because they are able to amass wealth.
        * Indulgence. They can hedge any decision, so risks become irrelevant.
        * High standards, perfectionism, since they can purge branches where things don’t go their way.
        * Resentment from the rest of society: they leave behind destruction, dead bodies, and inevitably leave each other and everyone else behind.
        * But ultimately they are tolerated, as the number of them in any one branch is relatively small. Even crimes are fairly mundane as eventually the culprit is likely to turn up dead.
        * Loneliness
            * Hylan and Danny are the best examples. Although they hide it from each other, they are desperately lonely (Danny to the point of suicide, Hylan to the point of divorce) even though they are in love. 
* Butterfly effect
    * Dr Paz was “unknowingly was responsible for all of this.” 
    * Whole New Phoenix plot is a chaotic result of Hylan choosing to go on vacation
    * Hedging with spins deals with the butterfly effect. _This should be made more explicit._ 

Secondary themes:



* Perfectionism
    * Lifeless dignity vs *something*
    * **Waiting (too long)**
* A critique of egoism “you spinners aren’t egoists at all”
    * Shows up with police woman. (no other character gets chance / motivation to argue a counterpoint.)
    * Maybe the mexican woman should do this instead. Or in addition to. Could make her version softer, more neutral.
    * “Youre just Suicidal”
        * Spinners should be faced with situation where they prune a world with no recent branches. Knowing that there are infinite distant realities and one has got to be better than this one.
* Superrationality
    * Shows up briefly in cantina with young spinner’s excited explanation of Prisoners Dilemma.
    * Could have a literal prisoners dilemma. 
