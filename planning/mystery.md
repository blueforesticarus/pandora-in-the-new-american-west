## Mystery

Several parts of the story go unresolved for a long time, or possibly passed the end of the story. This is a slightly more pronounced / macroscopic feature than checkov motifs, so I will treat them seperately. In fairness, this is a grey line \todo{is the coal car a checkov, or a mystery, its on the edge.}

The first distinction is: a checkov creates suspense around what *will* happen, while a mystery creates suspense around what *has* happened. They overlap, if solving the mystery is foreshadowed as something the character will do. 

- Who is the man in red? 
- What happened to money?
- Why are people walking into #13
- Who is following hylan? -> Lucy Armstrong

CORE:
- Why was cargo car swapped?
  - clarify #13 mess \FIXME
  - implied: nuclear material
- Why is president o'connors in New Pheonix?
  - implied: a deal with wilber

Side mysterys:
- rancho rancho
- cardinal = wilber
- narrator

The core answer is: Wilber uses the transorient for travel, transporting goods, and meeting associates. Upon arriving in New Pheonix, he delays their depature by having his people sabatage the train, and ensure repairs are delayed. President O'connors, likewise, makes use of the Lone Star Line. Once her train meets up with the Transorient in New Pheonix, wilber is able to complete his deal (relating to the replaced cargo car) and be on his way. The fact that car #13 is skipped on the official train manifest allows wilber to avoid its detection. The reason for Hylan's missing money, and the shifted appearance of the cargo car, is that the entire car was swapped out first thing on arrival. 

The new contents of the hold are strongly hinted to be nuclear materials, both by the narrators chapter near the end, and by the description of the boxes. The narrator's chapter also adds infor to O'Connor: the narrator's spinner friend (who dreamed of having a nuke) is implied to have went into politics and got as far as new orleans. O'Connors recently became provisional president of new orleans, when theirs "went missing". The Lone Star chapter heavily implies politics is full of spinners who spin to decide contentious elections.

Need to make selection bias clear. In any path there will be *some* politician who won all the spins and they would be the hotshot. \TODO

Another piece to the puzzle is the Man in Red's aquired identity as a Roman Catholic Cardinal, stationed in New Pheonix. This explains his invulnerability in the City, and rancho's inability to do anything about him. \TODO{care must be taken in chapter 20 with the "it was his city, wasn't it"} Realizing this ties back to O'Connor, who had somehow bribed her way into official sainthood.

Rancho's disappearance is as deliberately unexplained as the rest of his character. I am choosing not to come up with what happened, since I want the reader to have no way of guessing.
The options, plausibly, are:
1. He is dead or in hiding, and got the message to hylan in advance
2. The note is from wilber, to lure him out. (which he retcons as a test when Hylan survives)
3. Rancho is fine, completed his task and has been rewarded.
  - task was from man in red
  - task was from american government
  - task was personal (this one doesn't really make sense)
4. Rancho is fine, and is leaving Hylan alone so he can get on with his life
- It might be good to hint at these in Hylan's inner monologue, but be carefull not to be too explicit, since I think the reader can pick up on the possibilties. Also be carefull not to create expectation of an answer \TODO

**What is hylan's role in wilber's scheme** \FIXME
- "and when do we spin" "we already have" "and my role in all this..."  
- "circumstantial, and complete" 
