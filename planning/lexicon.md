symbolic allusion lexicon:

white oleander : symbol of woman's pride (up to and including self destruction)
    ex) I saw a tibetan woman cover herself in gasoline, and set it ablaze. Her flesh burned the color of white oleander.

instrumentality / evangelion / angels : TODO

kandelman's krim : placebo / hopium

margaret / margarita : the loving witch, persephone
   the kind of woman who could manipulate the devil

justina : I would use this name for any woman wronged by institutions.

vericose veins : a symbol taken from vonnegut
