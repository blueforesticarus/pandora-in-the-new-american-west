{
  description = "novel markdown workflow";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix-filter.url = "github:numtide/nix-filter";

  outputs = { self, nixpkgs, flake-utils, nix-filter }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let pkgs = nixpkgs.legacyPackages.${system}; 
						src = nix-filter.lib {
							include = []; 
						};
				in
        {
          devShells.default = import ./shell.nix { inherit pkgs; };
        }
      );
}

