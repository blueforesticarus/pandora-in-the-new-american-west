#!/bin/sh
# xdg-mime default edit.desktop x-scheme-handler/edit

# XXX depends on this line from my i3 config:
#     for_window [class="FloatTerm"] floating enable, resize set 1513 800, move position center
nvim="alacritty --class=FloatTerm --command nvim"

# This was replaced with trurl, still pretty handy though
# function urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

# check if we should open $1 in (existing) vscode instance
# 
# SPECIFICALLY: 
# checks if vscode was started with `code /full/path/to/folder/or/file` 
# for the file passed as $1
# 
# XXX this takes up alot of time
# `ps -C code --no-headers -o "args"` takes 300ms alone
function is_vscode_running(){
        A=$(
		ps -C code,Code,vscode --no-headers -o "args" |

		# grab the part after process path, starting with / (full file path)	
		rg ' /.*' -o
	)

	# Fail if no vscode process is found
	test -z "$A" && return 1

	# match each vscode process against file $1
        while read line; do
                (echo `realpath "$1"` | rg -F "$line") && return 0;
        done <<<"$A"

	# no match
        return 1
}

# Ripgrep version
function scan_rg(){
	# replace non-alphabetic characters with non-greedy catchall (.*?)
	# TODO is there a smarter way to handle spaces/newlines
	# NOTE removing `head -1` did not improve speed, but rg handles newlines badly
	PS=$( rg '[^a-zA-Z]+' --passthru -r '.*?'<<<"$TEXT" )
	PATTERN=$( head -1 <<<"$PS" | rg '.{30,}' )

	if [ -z "$PATTERN" ]; then
		if [ "$1" = "--try-harder" ]; then
			PATTERN=$( 
				rg '.{15,}' <<<"$PS" ||
				echo "$PS"
			)	
		else
			# give up
			return 1
		fi
	fi

	MATCH=$(
		# vimgrep include file,line,column for each match
		rg --multiline --vimgrep "$PATTERN" $FILE -g "$GLOB" |
		 
		# extract filename lineno and ~~column from first match
		# XXX: unicode breaks column https://github.com/BurntSushi/ripgrep/issues/2670
		head -1 | rg '(^.*:\d+):\d+:' -o -r '$1'
	)

	printf "$MATCH"
	test ! -z "$MATCH"
}

# REFACTOR
function scan_fzf2(){
	PATTERN=$( rg '[^a-zA-Z]+' --passthru -r "" <<<"$TEXT" | tr -d '\n' )
	
	MATCH=$( 
		# prefix each line of all matching files with 
		# -nH require linenumber and filename
		rg -nH '.*' $FILE -g "$GLOB" |
		
		# TRYING TO MAKE FASTER, BUT ITS SLOWER
		# this matches paragraphs, delimited by blank (empty or whitespace only) lines, 
		# and nul delimits them. Has to be be fancy b/c of filename/lineno
		rg '^([^:\n]+:\d+:[^\n\S]*\S.+\n)+' -U --null-data -o |

		# fuzzy find on the paragraphs 
		# XXX: this means we can't match on text across paragraphs
		# --filter is non-interactive mode
		fzf --read0 --filter="$PATTERN" |

		# extract filename and lineno from first match
		head -1 | rg '(^.*:\d+):' -o -r '$1'
	)
	
	printf "$MATCH"
	test ! -z "$MATCH"
}

function scan_fzf3(){
	PATTERN=$( rg '[^a-zA-Z]+' --passthru -r "" <<<"$TEXT" | tr -d '\n' )
	
	MATCH=$( 
		# match paragraphs, output null delimited with byte offset
		rg -H --null-data --byte-offset -o '(?:[^\n\S]*\S.+(?:\n|$))+' $FILE -g "$GLOB" |
		
		# fuzzy find on the paragraphs 
		# XXX: this means we can't match on text across paragraphs
		# --filter is non-interactive mode
		fzf --read0 --filter="$PATTERN" |

		# extract filename and lineno from first match
		head -1 | rg '^(.*?):(\d+):' -o -r '$1 $2'
	)

	[ -z "$MATCH" ] && return 1

	F="${MATCH% *}" #File name
	N="${MATCH##* }" #byte offset
	
	# get lineno from byte offset
	# BECAUSE WE HAVE TO DO THIS IT IS STILL SLOWER
	L=`dd if=blag.md bs=1 count="$N" status=none | wc -l`

	printf "$F:$L"
	return 0
}

function scan_fzf(){
	# remove all non-alphabetic characters
	# TODO is there a smarter way to handle spaces/newlines
	# NOTE: adding `head -1` did not improve speed
	PATTERN=$( rg '[^a-zA-Z]+' --passthru -r "" <<<"$TEXT" | tr -d '\n' )
	
	#exit 1.91
	MATCH=$( 
		# prefix each line of all matching files with 
		# -nH require linenumber and filename
		rg -nH '.*' $FILE -g "$GLOB" |

		# re-clear blank lines
		# NOTE: it would be nice if ripgrep let you insert NUL in replace strings
		rg '^[^:]+:\d+:\s*$' --passthru -r "" |
		
		# this matches paragraphs, delimited by blank lines, and nul delimits them
		rg '(.+\n|$)+' -o --pcre2 --multiline --null-data |

		# fuzzy find on the paragraphs 
		# XXX: this means we can't match on text across paragraphs
		# --filter is non-interactive mode
		fzf --read0 --filter="$PATTERN" |

		# extract filename and lineno from first match
		head -1 | rg '(^.*:\d+):' -o -r '$1'
	)
	
	printf "$MATCH"
	test ! -z "$MATCH"
}

function testfoo(){
	tt="`rg '[^a-zA-Z]+' --passthru -r "" <<<"$TEXT" | tr -d '\n'`"
	[ ${#tt} -gt 20 ] || exit
	
	m=`scan_rg --try-harder`
	m2=`scan_fzf`

	echo $m, $m2, $tt >> /tmp/vimlink.log

	if [ "$m" != "$m2" ] || [ -z "$m" ]; then
		echo "PATTERN $tt"
		echo $m, $m2
	fi
}

{
	echo "*** $(realpath $0) ***"

	# PATH HANDLING
	# NOTE: benchmark: trurl takes a not insignifigant component of total cputime (at least 1s out of 20s)
	# I though perhaps reducing the encoding burden would help (see dump2.tct) but I saw no change
	FILEPATH="`trurl --url "$1" -g '/{host}{path}'`"
	cd `dirname "$FILEPATH"`
	echo "DIR: `pwd`"

	# as interpreted by ripgrep, 
	# ex) FILE can be . or *.md both work
	FILE="`basename "$FILEPATH"`" 
	echo "FILE: $FILE"
	GLOB="`trurl --url "$1" -g '{query:glob}'`"
	echo "GLOB: $GLOB"

	TEXT=`trurl --url "$1" -g '{query:text}'`
	echo "TEXT: $TEXT"

	# I decided to add this because scanning for running vscode instance with ps slowed the whole thing down.
	EDITOR=`trurl --url "$1" -g '{query:editor}'`
	echo "EDITOR: $EDITOR"

	#exit 1.55s
	#set -x
	# NOTE: benchmarking reveals that fzf is faster for some paragraphs
	# specifically the first 10 in list (indicating perhaps it does better on smaller file)
	# adding `scan_rg ||` as first try : first 10 in dump.txt increase from 222 to 273 ms
	A="`scan_fzf || scan_rg --try-harder`"
	B=$( echo "$A" | rg '^(.*):(\d+)' -m 1 -o -r '+$2 $1' ) # for vim

	# NOTE: we can print them all and look if they are in order.
	# mostly in order is a good sign
	# echo "$A" $tt

	if [ -z "$A" ]; then 
		echo "*** FUZZY SEARCH FAILED ***"	
		#F="`rg --files $FILE -g "$GLOB"`"
		
		echo "$F"

		COUNT=`printf "$F" | wc -l`	
		if [ $COUNT -eq 1 ]; then
			$nvim -R "/tmp/vimlink.log" "$F" 
		else
			$nvim -R "/tmp/vimlink.log" 
		fi
	elif [ "$EDITOR" = "vscode" ] || ( [ "$EDITOR" = "auto" ] && is_vscode_running $A ); then
		xdg-open "vscode://file`pwd`/$A"
	else
		echo $EDITOR
		$nvim --server /tmp/vimlink.socket --remote $B +'exe "norm z\<CR>\<C-y>"'
	fi

} 2>&1 | tee /tmp/vimlink.log 
