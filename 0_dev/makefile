#! /usr/bin/env nix-shell
#! nix-shell -i make -p gnumake miniserve pandoc fswatch nodePackages.live-server gpp

build: out/draft.html
#build: all

DEPS:=$(shell find ch planning 0_dev -type f ! -path "*~")

all: $(DEPS:%.md=out/build/%.xml)

out/build/%.xml: %.md
	bash 0_dev/gen2.sh $< $@

out/draft.html: $(DEPS)
	@echo CHANGED: $? 
	bash 0_dev/gen.sh 

kill:
	-kill `lsof -i :30007 -t` 2>/dev/null

serve: kill
	miniserve -p 30007 . &

liveserve: kill
	# note, there is also https://github.com/lepture/python-livereload
	# added watch line because server was responding to changes in source files before fswatch built anything
	live-server --port=30007 --no-browser --watch=out/draft.html &

open:
	brave http://localhost:30007/out/draft.html 2>/dev/null

watch: unwatch
	fswatch `find 0_dev ch planning -type f` --exclude='out/' --latency 0.1 --event Updated | xargs -I {} bash -c 'sleep .1 && make build -j 32' &

unwatch:
	-ps | rg fswatch | rg -o "^\d+" | xargs kill

setup_mime:
	cd 0_dev/features/edit/; ln -snf `realpath edit.desktop` ~/.local/share/applications/
	cd 0_dev/features/edit/; ln -snf `realpath edit.sh` ~/.local/share/applications
	xdg-mime default edit.desktop x-scheme-handler/edit

install_systemd:
	cd 0_dev/systemd/; ln -snf `realpath *.service` ~/.config/systemd/user/
	cd 0_dev/systemd/; ln -snf `realpath *.socket` ~/.config/systemd/user/

_service: watch kill
	pwd
	live-server --port=30007 --no-browser --watch=out/draft.html

service: 
	systemctl --user daemon-reload
	systemctl --user restart pandora
	journalctl --user -u pandora -f
