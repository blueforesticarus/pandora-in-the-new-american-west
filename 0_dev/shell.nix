{ pkgs ? import <nixpkgs> { } }:
with pkgs;

mkShell {
  packages = [
     gpp
     pandoc
     fswatch
     nodePackages.live-server
		 miniserve
		 gnumake
  ];
}
