# TODO package https://github.com/ctrlcctrlv/humnumsort
mkdir -p `dirname $2`

set -x

FORMAT=markdown\
+lists_without_preceding_blankline\
+wikilinks_title_after_pipe\
+implicit_header_references\
+mark

TIME=`date +%s.%N`

# pandoc replaces src attr by inlining the file. when embed resources is used.

echo -e "\n\n<meta start filepath=\"$L\">\n\n" > $2

cat $1 \
| gpp -T --include 0_dev/macros.def \
| pandoc \
-f $FORMAT \
	-t html5 \
	--section-divs \
	--reference-location=section \
	--wrap=preserve \
	>> $2

END=`date +%s.%N`

TIME=$(echo "$END - $TIME" | bc)

echo "<meta buildtime=\"$TIME\">" >> $2
echo -e "\n\n<meta end filepath=\"$L\">\n\n" >> $2
