# TODO package https://github.com/ctrlcctrlv/humnumsort
mkdir -p out

set -x

# lock file incase build is invoked twice
exec 100>out/gen.lock || exit 1
flock -w 5 100 || exit 1

echo "lock aquired"

CHAPTERS=`find ch/ -type f -iname '*.md' | ~/.cargo/bin/hns`
PLANNING="planning/notes.md planning/chapters.md planning/locations.md planning/mystery.md planning/motif.md planning/inbox.md"

FORMAT=markdown\
+lists_without_preceding_blankline\
+wikilinks_title_after_pipe\
+implicit_header_references\
+mark

DUMP='out/input'

	 for L in $CHAPTERS $PLANNING; do
		 # pandoc replaces src attr by inlining the file. when embed resources is used.
			echo -e "\n\n<meta start filepath=\"$L\">\n\n"
      cat $L
			echo -e "\n\n<meta end filepath=\"$L\">\n\n"
 	 done \
   | gpp -T --include 0_dev/macros.def \
   | tee $DUMP \
	 | pandoc -f $FORMAT -t html5 -s --toc --toc-depth 2 --section-divs --reference-location=section --wrap=preserve \
   --css 0_dev/features/tags.css \
   --css 0_dev/features/base.css \
   --include-in-header 0_dev/features/deps.html \
	 --include-in-header 0_dev/features/query_machine.html \
   --include-in-header 0_dev/features/toc.html \
   --include-in-header 0_dev/features/word_count.html \
   --include-in-header 0_dev/features/anchor.html \
   --include-in-header 0_dev/features/backlinks.html \
	 --include-in-header 0_dev/features/scroll_percent.html \
	 --include-in-header 0_dev/features/percent_bar.html \
	 --include-in-header 0_dev/features/edit/edit_link.html \
	 --include-in-header 0_dev/features/export.html \
   > out/_draft.html

wc out/input
wc out/_draft.html

if test `wc -c < "$DUMP"` -lt `wc -c < out/_draft.html`; then
	cat out/_draft.html > out/draft.html
	touch out/draft.html
	rm out/_draft.html
else
	echo "err: build failed, out smaller than input" >&2
fi

rm out/gen.lock $DUMP
