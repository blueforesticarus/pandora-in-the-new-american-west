\chapter{23} 
> \META \OKAY

::: NOTE
Have rancho or other offer Hylan a game of poker. \TODO
Or this can be in ch25
:::

New Phoenix baked red and grey 8 hours out of the day. Morning and evening -- this time of year anyway. Have you ever noticed? How in the low hours of the sun everything bleeds into the background? Shadows are drowned out: more light refracts off the atmosphere then penetrates directly. Objects are illuminated from all directions, and become invisible. You lose your sense of depth. Roads become more dangerous, objects are harder to find, passersby go unnoticed. Still too bright out for flashlights to help. You can see everything, but discern nothing. Look it up: most accidents happen at twilight. \motif{grey-light} \GOOD

So unless you were paying attention, you probably wouldn't have noticed the blacked out train cars, the matching black SUVs or the secret service. You wouldn't notice and you wouldn't remember. Hylan didn't anyway.	

We are getting to a part of the story where I only have bits and pieces. Hylan presumably returned to the hotel. Got a call from Rancho. His waiter friend had disappeared by the time he was done checking out the Train. Hylan hadn't noticed anything different. Rancho told him what he had missed. 

"That's just it, *ese*, question is, _por que_?"

He was right. The train had not even begun repairs. I know because I was there. I had my own deals with the good detective to follow through on. The rest of this story I know through less orthodox means. Less _disclosable_. 

The key to it all was the cargo car: Car 13 of the TransAmerican Express -- Hylan hadn't even noticed at the time. If he had pieced it together sooner, he might have found his money, or he may not. Anyway, at some point someone did. If there wasn't something going on, they would have spun for it already -- but there was, so they hadn't. That was the logic Hylan used. There were 4 of them. All dressed as railroad mechanics. How much you'd have to pay a spinner to dress up like that! \motif{spinners} \motif{loc-cargo-car}

That was some time later though. 

Rancho and Hylan worked in close quarters the next several days. They surveyed the station, they followed up on leads. Hylan would sniff for information in places the detective could not, and Rancho would intervene before Hylan would have need of his Spinner box. Rancho needed him alive and present, though Hylan was acutely aware that their activities were biding time. Rancho came in person to collect him from the hotel. He was gracious with Danny, promised to return Hylan safely, tipping his hat and patting his revolver. Danny acquiesced. Her expression in recent days had become inscrutable to Hylan. She, too, was waiting, biding her time. 

When he returned in the evenings Danny would wait as Hylan went over the day's events, the clues, the pieces falling together. She never asked, but she expected to be told. She had become so quiet -- her talkative eyes had suddenly gone silent, and begun to watch. But certain words reached him: in her silence, in the way she nodded or shook her head or cocked it to one side in response to his stories, in the way she led him off to bed when he had told her everything there was to tell. In the way she, always playful in bed, had instead become entirely selfless -- like a concubine.

All of it was a message. A single wordless plea, repeated over and over. 

"Remember our agreement."

Or was it just in his head? Was she really just too tired of it all?

Maybe she had given up. \st{on him} Maybe she regretted all those things she'd said on the rooftop -- had gone up there meaning to say something else. \st{like he had}

It didn't matter though. Every evening maroon twilight lit her naked body. Every morning she awoke before him, made him coffee, scrubbed his back in the shower. Every morning she kept on her night slip, barely clothed, for when the good detective arrived. When he did she raised her eyebrows at him, gave him a look of steel, like a lioness. The detective would promise "he's safe with me, senorita," and she'd allow him to kiss her hand without looking at him. And before letting them leave she'd kiss Hylan goodbye -- softening completely, her attention turned, molding her body against his, arching her back, standing up on her toes, tilting her head back with eyes closed. A gesture of perfect longing and desperation. "I'm yours, come back to me", unsaid and unneeded, the strap of her slip falling off her shoulder. She let Hylan hold her firmly, her arms helpless at her sides, her nipples poking through thin fabric semi translucent in the morning light. A proud beautiful woman, in complete submission. What kind of man does it take? Detective. Who do you think you are dealing with?

What's it take to make a Machismo blush?

What's it take to make him sweat? 

The two men went off to work. 

"Buena mujer," Rancho said without fail.

"Si," Hylan replied.

Each day they pursued leads, questioned miscreants, asked about work, who knew who, heard about any jobs? They bought lowlifes drinks, flashed badges, brandished boxes, waved guns in faces -- *who's the _jefe, ese_?* But there wasn't much hierarchy in New Phoenix, everyone was out for themselves. 

They heard about a couple big shots. Old school cartel guys, relics really, hands in all sorts of business: bandits, thieves, racketeers, whores and their pimps, drug addicts and their dealers, ranchers dealing in more than cattle, cantinas selling more than booze. And other characters too: a city planner known for embezzlement, \allusion{a sly criminal lawyer}, a Roman Catholic cardinal -- who had among his friends every sinner of note in the whole of NeoMexico, etc. \motif{cardinal}

And then there were the spinners, with reputations so long and tall, you'd never find them. They went around with impunity, and Rancho seemed to know all of them. Sometimes they knew something, sometimes they didn't, and they'd give you a riddle for an answer either way. Somehow the detective fit right in. Badge for a rolex, glock for a weston -- no fancy sombrero, just a short brim cowboy hat. But you wouldn't notice is the thing, not in the haze. \motif{clothes} \motif{spinners} \motif{fit-so-well}

They all spoke a dialect of Spanish that Hylan had trouble replicating, and he eventually gave up, going back to New New York Spanglish: a creole good anywhere in western hemisphere, like Old Dollars in the not so distant past. "El Gringo." -- that's how Rancho introduced him. Nobody asked Hylan where he was from, just like they didn't ask if he was a spinner. \COMMENT It wasn't necessary. Sometimes too, Rancho would jibe "Boy, you should see his wife!" and he'd whistle, "Las gringas, ¿verdad?" And the men would all laugh, and nod, and Hylan would shrug: "Quizás, pero comparada las latinas..." He fit right in as well. Thick as thieves -- you might say. \motif{thick-as-thieves} \GOOD

\comment "They would not ask Hylan..." \ALT ch2 diction

They pursued these lines, trying to pick up the trail of the man in red. It seemed to Hylan they were getting nowhere, but Rancho absorbed information silently with an expression of always knowing a bit more than he let on. He led them from slum to slum, as if following some invisible trail which Hylan's American nose could not pick up. He could tell Rancho was getting close to something. 

"How many cars does the Transamerican have?" he asked one day. 

"Surely you..." 

"No, just try to remember." Hylan thought about it and told him. \INS{describe whole train\ALT}

As days went on Rancho had other duties to attend too. He was a detective after all. In their travels they witnessed a few crimes which he couldn't hold a blind eye too, and Hylan backed him up when he needed it. Rancho's partner had been left behind, put on paperwork, on account of secrecy -- their arrangement was strictly extralegal. Still, Hylan helped him detain a delirious drunk, who'd stabbed a whore to death in the alley. He'd gunned down a garifuna man who'd gotten the jump on Rancho and went for his revolver. He was a dealer, it seemed: a misunderstanding, they were only here to ask questions. 

I'm sorry, did I give the impression New Phoenix was a nice place? It wasn't. \GOOD \motif{narrator}

Rancho attended to more of his everyday duties and Hylan agreed to accompany him. He had nothing else to do, there was no surer way to his goal. Any questions as to his money were evaded.

"You haven't figured it out yet, Agente Especial? You gotta pay more attention. Without that thing," he pointed at the box, _"you'd be dead a long time ago."_

Ordinarily, Hylan might have wondered, if when all was said and done, it would turn out to be Rancho and the police who had it all along -- except the timing was impossible. Hylan hadn't ever stopped to think about it, but really, it made very little sense to begin with; he had taken every measure -- yet still, the money had been found, almost immediately. \motif{sane-measures} Even for Wilber, it raised a series of uncertainties -- and for the most part Hylan would just have to acquiesce to their company. 

There *was* one day, while having lunch at a cafe in a nice part of district 4, \loc{d4} Rancho relented and gave up some of the details. They were sitting inside by the window. Hylan wore \todo{blue}, sunbleached and crowned with dust, cheap sunglasses perched on his nose -- really, the lenses were too small to be much use for anything, and they were meant as a novelty, but he kept them on anyway. Rancho, for his part, maintained his typical detective's grey, his aviators set on the table. \motif{sunglasses} \motif{hylan-suit-color} 

\NOTE sunglasses cannot be removed without weird flow.

"There", he pointed at absolutely nothing. "*Invisible como...* How do you say? *in plain sight*." Hylan glared and Rancho laughed, stopped, and grew dead serious. "Four men, recently hired as mechanics -- and yet no work gets done. But if you don't wait, you will never get all of it. They work for Wilber." 

_And when he's gone, there will be nothing left to do but spin for it._

Rancho never gave away much more then that, and Hylan was never introduced to any of the other officers nor any of Rancho's less official agents -- or at least they weren't introduced as such -- and Hylan never saw any more of that waiter from Bar Fuego. 

Even still, he noticed from time to time he was still under surveillance, though whether for his protection, or because he (obviously) could not be trusted -- that remained unclear. Hylan never brought it up; he didn't need to insult the man.^[a good spy is hard to come by you know] He figured, he had a keen eye was all. 




Hylan for his part, hid little and volunteered nothing. He saw little point in either, Rancho seemed to know everything already. That didn't always stop him from asking. \WORK

\NOTE could insert "needed to get away snippet" here \ALT

"What does he want? Wilber I mean." Hylan asked. "What's his long game." 

"*Buenisimo* Hylan, that's the question!"

"I figured you knew."

"Puzzle pieces, Hermano, puzzle pieces."

"And what do the puzzle pieces say?"

Rancho ignored him. "Well, lets flip it around, what is it *you* are after."

"You already know."

"After that, after that. What do you actually want?" \WORK{too much}

Hylan thought a moment, and Rancho probably figured he wouldn't answer.

"A good story, I suppose." Hylan said. \motif{want} 

"Seems you're in the middle of one -- at a minimum."

"Its only a story when its over". Hylan said. \motif{story} \WORK

Rancho shrugged. "A story is a story. Life is life. At any rate, you've got the right man -- you want a story, he's your \allusion{Lee Van Cleef}." \weak{jack}




***


At a point it seemed they had stopped pursuing Wilber entirely, and Hylan wondered if really it was all a bluff, and Rancho had just found an amusing way to recruit a partner. "Lo siento por su dinero amigo. ¿Quiéres un trabajo? Paga mala, pero las muchachas: les gusta la uniforma." Or maybe Rancho had plans of his own. 
Stories are built on archetypes. There are only so many kinds of criminal and police. The jaded ex-cop, corrupted by all he has seen -- the bastard thug, who always had to scrape by -- rebels, anarchists, those that could _never_ be cops, from the bottom of their souls, until they are -- and of course, those that currently are, good, bad, on and off duty, rookie, veteran, crooked and/or internal affairs. 
Hylan, for his part, couldn't -- could have, maybe, once upon a time -- but then he found the box, and the box had all the benefits of a badge and more. 
Ultimately, Hylan suspected the real story was much simpler: they were just waiting, killing time. He was bait, though the how and why eluded him. As did the when and where. And who. Who was the Man in Red?

None of these questions would be answered. \GOOD

One day Rancho did not come. Hylan waited -- waited until past noon. Eventually he decided to chance it, or rather, the box decided (there *are* alot of 1s in this story, aren't there?), \motif{binary} and he went looking at the police station. Danny came along -- she didn't ask. 

Down the stairs, to the basement -- Hylan asked the receptionist to speak to Detective Rancho; said he had remembered some new information about the case.

"Detective Who?" she asked, then suddenly, "Oh!" she laughed, "Right, Detective _Rancho_, of course," rolling her eyes. "He no longer works in this department. He was promoted." 

She offered to call his new office, but nobody picked up, so she sent them off with an address halfway across the city, and went back to watching TV on her phone. 

***

District 105. The lakes out that way have no slums, nor names. It took hours to reach the address. Danny rested on Hylan's shoulder taking the N.W. CrossTrack, looking out the window. Hylan looked at the ceiling of the tram. Riveted sheet steel, not too different from the ceilings of the shacks which were becoming more and more sparse outside the train, on the other side of the sandwall. This area was too new and too barren for poverty.

The sun was still high in the sky. Rancho's new office was in a new building in a new neighborhood, the top floor of a concrete modern. That was all there really was here, that and the occasional glass tower, shooting up above the canopy. The road ran roughly east-west. On the north side the deserts were _tiny,_ scarcely a mile across, and you could see dozens of them. They pockmarked the landscape. To the south was one enormous desert, _Sonoran-C._ Technically it was not a district, rather, it was the negative space that districts were carved out of: a piece of the desert that they had not even started to reclaim... first generation children of the Sonoran; it was here that her spirit was most alive. Sandstorms plagued the border areas, even with the sandwall. The streets were not nearly so clear as those twice daily swept in Once Pobre, or any of the other central districts. It was nice out today, though -- the sky was a little clearer out here than in central, when wind blew in a favorable direction. \COMMENT Today was such a day; the sky was clear, and little puddles of sand crunched under foot. 

\comment symbolism? why is the weather good? Kind-of a urban-grit vs suburban-plastic thing.

They said the Detective wasn't in at the moment, but they could wait in his office. He had a lot of windows, wooden bookcases filled with legal texts and knick knacks and a few awards. His diploma \st{so thats your real name} and certificates hung on the walls. In the center of the room was a large mahogany desk -- on closer inspection, veneer -- and a high backed black vinyl office chair, two smaller ones on the other side. They sat down automatically, looked at each other. Neither one had any idea what to make of it.

Awhile later the secretary came back in, flustered. 

"I apologize, there's been some miscommunication, the coordinator won't be in today. You will have to come back tomorrow," she said. "Would you like me to let him know you are coming? Mr...?"

"Rancho," he said with a sly smile, "Mr. and Mrs. Rancho. Yes please do." 

The secretary nodded and held the door as they left.

Back on the street, they headed for the station. With very few answers, they had run out of questions -- there was nothing to do but head home. They came across another couple on the way -- these two did have questions; namely how to find a train. They wanted to get north-east and were in the wrong place, so Hylan and Danny sent them on their way over to the far side of the axon (though truth be told that wasn't very far out here). Hylan could tell by the accent they were natives, probably from some next town over... \allusion{Mexicans lost in Mexico}. He and Danny made their own way to their own station, back to the N.W CrossTrack to catch a S.E train bound for *Central*. They got there just as it was boarding. 

***

Greylight came and the sun dipped into crimson. This time Danny slept, and _he_ watched the desert, looking straight ahead out the opposite window of the train. \motif{grey-light}

District 99 

District 111 

District 97 

District 96

District 81

District 94

District 92

District 35

And so on. It was a long way back. They had gone more than halfway to Tijuana. 

They could have kept going, he supposed. Could have had a little vacation on the beach. Figured it out from there, like they always had. He had more than enough money in his pocket, and he had infinite opportunity resting in his bag. "Remember our deal," said the girl, sound asleep on his shoulder.

You didn't move backward. That wasn't how it worked. And you certainly didn't get stolen from.

They passed alongside the heavy rail, whizzing past another train -- a full size locomotive -- coming into the city, heading for Central. It's carriages were old-fashioned, but after some other decade -- it wasn't the TransAmerican, only the cargo cars looked the same. \motif{lone-star} \OKAY

Hylan nudged her awake when they reached their stop. She stayed awake the rest of the way, holding his hand on the #14, the #17, the #11 back to the hotel. When they arrived Danny made a beeline for the bed and flopped down. After a few seconds she jumped back up, marched over to the mini fridge which Rancho had ordered stocked for them, tore the cap off a corona and drank it in a single breath. Then she flopped back into bed to sleep. Whatever they did from here on could be discussed in the morning. 

In her exhaustion, she hadn't noticed it when she opened the door. In fact she'd stepped right on it. In truth Hylan might have missed it too, and they would have found it together in the morning.

But he did not miss it. And while Danny was chugging her beer he picked the small card off the floor. 

| Guillermo Gibieron \st{william gibson}
|
| Niel Estévez \st{neal stephenson}
| 
| Felipe Q. Díaz \st{phillup k dick}
|
| Mateo Papasquiaro Paz

No other text. None was needed. \motif{none-were-needed}
