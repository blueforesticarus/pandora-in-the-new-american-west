\chapter{18}
> \META \GOOD

<img src="img/door.png" class="chapterhead">

::: NOTE
Probably still want to target 800 worlds \WORK
:::

At the edge of space and the end of time, around a bend and in the trough of two 20ft dunes gliding several feet per minute, Hylan came across a crude wooden cross, tied together,  leaning out of the ground, a ribbon of excess fabric twitching in a breeze which had suddenly all but subsided. The sand became more shallow, and Hylan staggered past more crosses over shallow piles and splotches of hard ground. Ahead, around another hazy bend and half buried in sand was the black speck. 

It was a cottage, somehow preserved. Its terracotta roof, so dusted with sand it was indistinguishable, and its mud walls -- painted black. The door was mostly cleared away with sand. Inside were three chairs, two occupied. They were dressed as bullfighters, their sombreros tipped over their faces. Their hands were burnt -- they were no longer of this world. 

There were three rooms in the cottage. The main room with the round wooden table and the three chairs and the two spinners and a pile of used boxes left behind. In the second room Hylan found a few discarded canteens, which he used to refill his own before having a look at the boxes. He only had to check a couple to find one in working order. It had a "cut" button too, which Hylan pressed a few times, no sparks, and he saw random numbers pop up on the diag console. He’d need it to get back: he was down to his last cigarette, \motif{cigarettes} so to speak. He returned to the canteen pile, found another with some water still sloshing around in it and drank till it was empty.

Next he searched the spinners for anything that might indicate they had something to do with his missing money, the train breakdown, or the Man in Red. \motif{man-in-red} But there was nothing. They were most likely cattle hustlers or jewelry thieves or con artists or just gamblers -- there was no shortage of ways to make a living flipping coins, in a city like this. The third room didn't have much, just an old child's bed, a desk and a stool, some notches marked in the doorway -- the lines and dates, “2/4/17 -- Mario”,  “4/5/19 -- " and so on. \motif{paz} \location{hideout}

Hylan would have to deal with the bodies, per the custom. He looked around for a shovel and sure enough he found one by the door. The sticks he found in the second room, and the scraps of cloth he tore from the spinners clothing. Nice cloth, bright, good quality. He took a long sip of water, and stepped back into the sun, finding a yet unmarked place some 15 paces away. He dug two graves -- not deep, but deep enough, past the hard layer. For each he fashioned a cross, tying the sticks together and stabbing them into the earth. When he was done he trudged back inside, taking each of the cadavers by the arms, one by one, and dragging them into the hole. Then he buried them. 

He left the cottage with the box and a full canteen, plus a sombrero borrowed from one of the spinner, the tan suit and the sunglasses he'd bought before \motif{hylan-suit-color} \motif{sunglasses}, and headed back the way he came. He came to a fork in the dunes -- "1", said the box, so he went left. \motif{sombrero}

