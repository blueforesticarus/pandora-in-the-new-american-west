\chapter{19}
> \META \GOOD

:::NOTE
philosophical breaking point, hylan metaphorically kills doubt should have some thing where hylan doubts if he will ever find his money to better set up next chapter After this tone gets lighter, hylan is more confident, \TODO

what do you know, chapter 18 is currently starts exactly half way through the book.
:::

\fixme{The train back was uneventful.} Hylan sat with his left hand on his new box by his side. The same woman as before was on board. No muchacho this time, though she seemed to be stealing glances at Hylan instead. 

It was dusk. The city burnt an amber hue and skyscrapers flitted in flame where the sun drowned in the west, while east succumbed to ashen night. The creatures of day gave way to creatures of eventide.^[though they were the same creatures], and it seemed, as it was, inevitable: that tomorrow would be a new day, a new branch, a new phoenix.^[but I repeat myself \motif{phoenix}] It was the kind of light that played tricks on you. For a moment he thought he saw a pair of something, maybe mole rats... but they were too big and too far away to be mole rats -- something traveling out in the desert, but the train bumped and they were gone. \COMMENT \motif{two-men-cross-desert} \motif{grey-light} \motif{phoenix}

\comment mole rats works with el topo reference, and mole rats are blind, adept at living underground... cheap symbolism \motif{two-men-cross-desert}

Fifteen minutes went by and he reached his stop, the woman smiled at him and made to get off. Hylan followed. The station was mostly empty and he crossed the platform to wait for the #11 -- back to the hotel, back to Once Pobre, back to Danny who surely wasn't there. Probably out dancing with god knows who and without a care in the world -- best case. \motif{foreshadowing}

The woman was taking the number #11 as well. Hylan smiled at her and she blushed. She was wearing jeans, a jacket that was just a little too new, over a low cut shirt showing off mediocre breasts... She sat closer this time, and Hylan caught a whiff of perfume. 

Hylan's stop came and went, and he circled halfway around the desert. He got off instead at the north edge and transferred to the #4. The woman gave him a look, and got off with him. \loc{d4}

"Hey" he said. "Like a drink?"

She nodded.

They started walking to the other side of the axon, away from district 4 -- made their way silently past the dark streets and glass towers, the signs and shops, groups of rowdy boys and giggling girls and couples, silent concrete highrises, the railing overlooking District #2 -- _Medio_ -- beyond the sandwall, past adobes and shacks and beggars and addicts... and spinners. \motif{ch1-cadence} \loc{d4} 

He would have to cross the axon back over to the #4 to leave. For whatever reason, the trains don't run backwards from the #2. \loc{d4} \WORK

Hylan led her to a cantina he had found days before. She followed him easily, she didn't look uncomfortable; he was surprised, or maybe he wasn't, maybe it made sense. She did hesitate a bit before following him into the bar.

"They know me here; you'll be safe with me. You will like this place. Very authentic. You are a tourist right?"

She smiled and nodded, having made up her mind. They went into the bar. It was dimly lit, only a few rough characters, staring into their bottles, a bartender in the back -- the owner, surely -- watching them come in. He watched as Hylan led her to a table, watched them sit down. *She* seemed to be surveying the room, but Hylan gave her a disarming smile and she stopped, batted her eyelashes at him, suddenly relaxed. 

They could overhear a conversation from up at the bar: \TODO{formatting}

"Yeah, I hear someone's lookin' for a job done. But its a weird one, normally not my business but..." \motif{cardinal} \OKAY

"No, I heard. I reckon not many out there are ichin' to cross *el obispo*"

"Hold on, what I hear *he's* the one that put out the call."

"Nah, they's sayin it was someone else, \alt{*un británico...*}"

...

The bartender still had his eyes on them. Hylan shot him a glance, but that was all. He returned his focus to the girl. \OFF

Despite her demeanor, she seemed to have trouble holding eye contact. \OFF

"Brave of you to come out here," Hylan said.

"I've been accused of recklessness," she replied, "especially when it comes to chasing men."

"Oh? Think I'm who you're looking for?"

"You fit the description," she said, looking him up and down. 

Hylan wondered what was going through this woman's head. Whether she really had any idea where she was, what kind of people these were, what kind of person *he* was, for all she knew. He looked at her incredulously -- she didn't notice. Maybe she was just bold, maybe she was stupid, maybe...

"So, are you going to buy us some drinks?" she asked.

...no, _pretended_ not to notice. 

"They bring it to you here," he said.

"Really? The bartender.." her affect slipped.

"Mm hm," Hylan nodded. 

"Are you sure?" she reached for her purse "Why don't I just go up there and..."

Hylan growled. "I told you, they bring it to you here." 

And then she heard the click. No, she _recognized_ the click. She knew what it was -- she shouldn't have known what it was. A ditsy American tourist wouldn't have known, her character wouldn't have known. But she did -- it was all over her face -- her fate was sealed as soon as the blood drained from her cheeks. A tourist wouldn't know what it was, a spinner wouldn't be afraid... She sat back in her seat, rested her arms back on the table. 

"So..."

Crash! \COMMENT A few tables over a drunk fell over his stool and Hylan looked away -- long enough for the woman to make a break for it. But as soon as she stood up Hylan had the gun on her again and she froze. 

\comment might be better if she starts pointing the gun at him and this is how Hylan gets the jump on her \ALT

_A spinner would have gone for it._ Chances are a spinner wouldn't even be here. \motif{spinners} There was silence for a few moments.

"We got a problem, Muchacho?" The bartender had a shotgun trained on him, must have had it under the counter. 

"No problem," Hylan said, as he slowly placed his bag on the table, and tossed open the sides so everyone could see the box. 

_A spinner always has a way out._ \motif{spinners}

_A spinner always has contingencies._ \motif{spinners}

_A spinner can burn your place to the ground from beyond the grave._ \motif{spinners}

_Can and will._

_Can and will and probably already did._

_A spinner doesn't lose._ \motif{spinners}

"What about you, Hermano. We good?" Hylan said, back turned to the bar and the shotgun aimed at him. The bartender lowered the gun. Hylan sat down without checking to see if he had. He gestured for the woman to do the same. The bartender returned to cleaning bottles; he would not make eye contact with her. If anyone else in the place was even conscious, they made no show of it. 

"Name?"

"Lucy Armstrong." She almost looked like she expected that to mean something.

"NNYPD?"

"FBI." Almost offended.

"Department?"

"High profile"

"You working with Rancho?"

"Who?"

"This is about Wilber? Right?"

"Who??"

"The spinner? All red suit?"

"I'm not familiar with anyone of that description."

Hylan looked at her... incredulously. 

"Why... Why are you here?"

"You."

New problems.

"Purse." She handed it to him. He poked through, careful to keep the gun on her. Make-up, tape recorder, cell phone, a Glock 19 which he \work{\del{disarmed and unloaded and} tucked into his belt}. Badge... shockingly, she was who she said she was. He took the cell phone, tossed it out of sight -- the rest he dumped in his bag.

He had to decide if he believed her. He didn't think Rancho had a reason to use a clumsy FBI agent outside her jurisdiction to tail him -- he probably had his own tails (Ones he wouldn't have noticed so easily). She really didn't seem to know who Hylan was talking about with Wilber, and at this point she was either a bad actress or a spectacular one. All reason suggested she was here for him. She must have followed him here on the train, maybe since Austin, maybe even as far as New New York. Hylan himself had caught somebody's attention, he was moving up in the world. Then again, what was an FBI agent doing in Mexico? Maybe she had gone off on her own. Maybe it was just her. 

Hylan motioned for her to get up, they were leaving. She stood up slowly. He motioned for her to turn around. She did, eyes closed. He pushed her out of the cantina, gun to her back. Neither spoke. It was perfect silence besides the soft sound of footsteps. He walked her down the alley. Past adobe's and shacks and bums sound asleep. He walked her about 50 paces into the desert and stopped. 

"Turn around." She did; tears were welling up around her eyes. "Relax. Spinners don't kill in cold blood." \motif{spinners}

She scoffed, hopelessly. "What do you want?" \motif{want}

"That's my line." Hylan replied.

"What do you think?!"

He gestured to her to go on.

"Fine, on Oct 9th you and 2 others, a certain Jeb Li and Hill Gruggs committed an armed robbery of the First Orthodox Private Bank in NY3, to the tune of 2.9 million new-dollars, working for a fixer by the name of Donald Glenn. We recovered the bodies of the others, along with Glenn's son. That leaves you."

"And you were sent here after me?" Hylan asked.

"Yes" -- a tinge of doubt in her voice.

"Over a measly 3 million new dollars? From some Eastern European bank?"

"The bureau," she hesitated again, "is aware of your *prior* exploits."

"Are they?"

"I think I answered." She said.

She had the same verbal tick as before. Hylan tapped his forehead with the flat of the barrel, thinking -- no, it didn't matter. 

"My money?" Hylan said.

She gave a confused look.

"sorry to disappoint you, but I don't have it," he said.

"What, did you get mugged or something?"

"Something like that."

"Haven't criminals heard of banks."

"This shouldn't surprise you, but I don't have much faith in banks."

"So where is it?" she asked.

"I don't know. Maybe you do."

"I don't know anything."

Hylan straightened the gun.

"Really! I don't," she insisted.

He squinted at her, then looked off in thought.

"But maybe you could," Hylan said.

"Huh?"

"That's what your after too, isn't it?"

"Evidence," she said sternly, "I'm trying to recover what was stolen."

"Not my business," Hylan said. 

Silence passed for a while. They were staring right at each other. Finally Hylan made a decision. 

"I think we can work out a deal."

"What?"

"We are both after the same thing really. And we have a better chance of finding it together. You tracked me here, so you're not as incompetent a detective as your cover acting would suggest. Besides, you can't get me on anything without that money, it's the only evidence you've got." 

"And what, then you turn yourself in?" she asked incredulously.

"We spin for it." Hylan said "You get to bring my body with the cash to your superiors, and I get you off my back. We both win."

She didn't say anything for a while. 

"What on earth do you think you're talking about?"

Hylan started to explain. "I've got this box, see..." 

"I know how it works," she snapped. "How could you possibly think I'd agree to that? Agree to work with you, to use that... thing. It's sick, it's deranged."

"It's physics," Hylan said.

"Physics, right. Physics. Physics says it's okay to get fried up. To litter dead bodies everywhere. You know how many of you I have to scrape up every day."

"Yet here I am." 

"Yeah, holding a gun in my face offering a duel to the death casually as if you were asking me to coffee." \motif{duel}

Hylan looked at his gun self consciously. He lowered it to his side and sighed.

"You'd have to be a spinner to understand." \motif{spinners}

:::NOTE 
- [x] stages if grief (already kinda done)
- [x] "we all pay for things" "spinner doesnt have to pay for anything" laughs as she was talking about him 
:::

\INS{build up} Suddenly she was furious. "Don't you dare! Don't talk like you know something we don't. Like you spinners have each other's backs. Like you are some kind of community. All you spinners ever do is leave each other behind, leave all of us..." \weak{fucking-spinners} \motif{lucy}

But she trailed off. 

The wind blew and Hylan watched her, but she refused to make eye contact. She had turned her head to stare off into the darkness -- thought's appearing and disappearing on her face. 

"I suppose we all pay for things." She mumbled to herself. \motif{faust}
 \ALT

"Everyone but us," Hylan replied, "\work{a spinner never has to pay for anything.}" 

She let out a defeated laugh. 

A long pause. \WORK

"You aren't going to give me a choice," she added finally.

"No, I suppose not." 

Hylan tossed her things out of his bag onto the sand. Feigning hesitation, he left the glock as well. \WORK{expand, tone}

"I'll meet you tomorrow at the #14," he said, and he began to walk out of the desert -- hoping in the most basic and obvious spirit of cooperation that she would let him. It took a moment, she hesitated, but of course there was no deal to be made -- this time *he* heard the click. \motif{selfish}

He turned around, she had the gun and a well practiced glare of authority, her voice as well-- 

"I'm placing you under arrest."

Then some kind of doubt flashed across her face -- something off -- maybe she noticed the weight was wrong. Hylan closed the distance between them in a blink and snatched back the gun; knocking her to the ground and drawing his own. He inspected the glock. She didn't even pull the trigger. A spinner would have, a spinner wouldn't have hesitated. Not that it would have made any difference. \motif{spinners}

Hylan muttered under his breath... "selfish bitch." \ALT \motif{selfish}

No choice now. Hylan kneeled and gripped the conduit of his box, pushing the button once: once for him. In the world where he was fried, we can assume Lucy Armstrong brought his body back to the local police, sent word home that the suspect was dead and the money was lost. Some supervisor probably offered halfhearted praise for a job well done, trying to raise her spirits, then chewed her out for leaving her jurisdiction all on her own, breaking protocol, putting herself at risk. The money, already long since reimbursed from some insurance pool, on loan from some bank, on loan from another bank perhaps itself on loan from the first, all eventually forgotten and erased in the eddies of finance, replenished off the tireless printing presses of the New American Dollar. Meanwhile, in the reality where nothing happened:

Two pairs of footsteps led 50 paces into a desert called _Medio_. The moon shone through the clouds of dust and basked dunes in pale light that refracted off the haze, murky like a fog. It was already past midnight. But as they say, it's always high noon somewhere. \loc{d4}

\alt{Four} shots rang out in the desert.
