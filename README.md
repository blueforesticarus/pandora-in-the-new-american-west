This is the git repository for my novel: **Pandora in the New American West.**
I'm making this public so that I can share my workflow and tooling for writing and revising in markdown.

0_dev contains the gen script and all custom markdown/html stuff

**many things broken for firefox, needs chromium based browser**

live example: <https://sonoran.xyz/public/pandora/out/draft.html>

![edit_links](0_dev/screenshots/prototype.webm)

# what it does

- **annotations** I use gpp to define and expand latex style macros before pandoc is executed. Macros are defined in 0_dec/macros.def. I define various macros for revision state `\GOOD`, `\TODO` etc, which optionally accept more detail ie. `\TODO{punctuation}` and in their lowercase forms specify the novel text to which the tag attaches `Bob heard the news and \todo{ran as fast as he could}{Bob hurts his foot earlier. he can't run}`. Pure css (including alot of modern selectors) is used to style the display of these tags. Several other types of annotations exist as well (add del sub motif location ins note). After porting my manuscript from google docs and spending several days revising within this workflow, I believe the current set of annotations is pretty close to *nessecary and sufficient* for how I work. 
- **out of band comments** To keep long comments seperate from novel text, I implemented 2 complex macros. 
    - `\COMMENT` specifies a comment anchor (like a footnote link)
    - `\comment` specifies a line as the body of a comment.
  these are paired, so the nth COMMENT points to the nth comment. I tend to place comments for a paragraph in the immediately following paragraph.
- **sidebar/TOC** moves pandoc generated TOC into a fixed sidebar, making navigating the document easier.
- **backlinks** for lack of a better term. 
 Paragraphs and spans annotated with `\motif{<name>}` to get copied into a `<name>` section at the end of the document. With links back the source. I use this keeping track of everywhere that a motif is used. I also build a list of links to the motif sections and put it in the right sidebar.
- **edit links** -- Javascript injects links displayed to the left of paragraphs. I use a custom xdg url scheme handler, plus rg/fzf based fuzzy finding, in order to open either vim or vscode to the correct file and location. This works *surprisingly well.*
- **url based settings** -- query_machine.html syncronizes the URL search/query args with root element html attributes **and** an html form. This is used to define parameters (such as hiding the sidebar or selecting vim or vscode) which can effect css styles, js behavior, while persisting on reload and being controlled in the ui.
- **live editing** -- see makefile for some simple workflows for live rebuilding and live refreshing the page on source changes.
- **word count** -- maintain a total and per chapter word count.

These features are largely independant, defined in their own files, and have no dependancies besides jquery.

## screenshots

![Start of an annotated chapter](0_dev/screenshots/1.png)
![Start of a chapter with a large note block at the start](0_dev/screenshots/27.png)
![Planning document generated toc with meta tags](0_dev/screenshots/toc.png)
![Chapter index + revision state map](0_dev/screenshots/chapters.png)
![Planning document motif section with backlinks](0_dev/screenshots/planning.png)
